<?php

/**
 * 管理员后台左侧菜单项的样式名
 * @Author zhanghong
 * @Date   2017-01-07
 * @param  array      $current_ancestor_ids 当前页面和它的所有祖先元素的ID
 * @param  array      $menu_item            菜单项
 * @return string                           Style Class Name
 */
function left_menu_item_class($current_ancestor_ids, $menu_item){
  $class_name = "";
  if(in_array($menu_item["id"], $current_ancestor_ids)){
    if(empty($menu_item["children"])){
      $class_name = "active";
    }else{
      $class_name = "active open";
    }
  }
  return $class_name;
}

/**
 * 管理员用户的头像路径
 * @Author zhanghong
 * @Date   2017-01-07
 * @param  array      $user User Object
 * @return string           头像路径
 */
function manager_avatar_path($user){
  $img_path = "/static/ace1.4/assets/images/avatars/user.jpg";
  if(!empty($user) && !empty($user["avatar"])){
    $img_path = $user["avatar"];
  }
  return $img_path;
}

/**
 * 管理员用户的显示名
 * @Author zhanghong
 * @Date   2017-01-07
 * @param  array      $user User Object
 * @return string           显示名
 */
function manager_shown_name($user){
  $name = "";
  if(!empty($user)){
    if(!empty($user["nickname"])){
      $name = $user["nickname"];
    }else{
      $name = $user["name"];
    }
  }
  return $name;
}

/**
 * 上传单张图片--系统自带的旧方法
 * @Author   zhanghong
 * @DateTime 2016-05-04
 * @param    string     $backcall 回调字段名
 * @param    integer    $width    图片高度
 * @param    integer    $height   图片宽度
 * @param    string     $image    当前图片路径
 * @param    string     $backcall_name    回调字段ID
 */
function upload_image($backcall="image", $width=100, $height=100, $image="", $backcall_name=""){
    if(empty($backcall_name)){
      $backcall_name = $backcall;
    }
    echo '<iframe scrolling="no" frameborder="0" border="0" onload="this.height=this.contentWindow.document.body.scrollHeight;this.width=this.contentWindow.document.body.scrollWidth;" width='.$width.' height="'.$height.'"  src="'.url('admin/upload/create').'?width='.$width.'&height='.$height.'&backcall='.$backcall.'&image='.$image.'&backcall_name='.$backcall_name.'"></iframe>
         <input type="hidden" name="'.$backcall_name.'" id="'.$backcall.'">';
}


