<?php

namespace app\admin\controller;
use think\Loader;
use think\Config;
use app\common\model\Comment;
class CommentController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new Comment();
  }

  public function index(){
    $param = $this->request->param();
    if(empty($param)){
      $param = [];
    }
    $res = $this->Model->managerArticleSelect($param);
   
    $this->assign("param", $param);
    $this->assign("res", $res);
    // return json($res);
    return $this->fetch();
  }

  public function show(){
    if($this->request->isPost()){
      $id = $this->request->param("id");
      $id = intval($id);
      $this->Model->where("id", $id)->update(["status" => 1]);
      $this->success("评论显示成功");
    }else{
      $this->error("请求的页面不存在");
    }
  }

  public function hide(){
    if($this->request->isPost()){
      $id = $this->request->param("id");
      $id = intval($id);
      $this->Model->where("id", $id)->update(["status" => 0]);
      $this->success("评论隐藏成功");
    }else{
      $this->error("请求的页面不存在");
    }
  }
}
