<?php
namespace app\admin\controller;
use think\Controller;
use app\common\model\Composite;
class CompositeController extends BaseController{
  public function _initialize(){
    parent::_initialize();
  }

  public function index(){
    return $this->fetch();
    //$this->redirect("manager/index");
    // return $this->fetch();
  }
  
  public function synthetic(){
    $res=array();
    $res['res']=0;
    $res['msg']="失败";
    $slogan = $this->request->param("slogan");
    if(!empty($slogan)){  
      $Model = new Composite();
      $res = $Model->generateRececertImage($slogan);   
      $res['res']=0;
      $res['msg']="成功";
    }        
    return $res;
  }
}