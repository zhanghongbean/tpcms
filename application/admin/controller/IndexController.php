<?php
namespace app\admin\controller;
use think\Controller;
class IndexController extends BaseController{
  public function _initialize(){
    parent::_initialize();
  }

  public function index(){
    $this->redirect("manager/index");
    // return $this->fetch();
  }
}