<?php

namespace app\admin\controller;
use think\Request;
use think\Session;
use think\Loader;
use think\Cache;
use think\Config;
use think\Db;
use app\common\model\AuthGroup;
use app\common\model\User;
class ManagerController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new User();
    $this->user_type = "manager";
  }

  public function index(){
    $param = [];
    $param = $this->request->param();
    $res = $this->Model->managerSelect($param);
    $this->assign("param", $param);
    $this->assign("res", $res);
    return $this->fetch();
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $res = $this->Model->addUser($data);
      if($res["id"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $GroupModel = new AuthGroup();
      $groups = $GroupModel->selectTypeAll($this->user_type);
      $this->assign("groups", $groups);
      return $this->fetch("form");
    }
  }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read($id)
  {
    $user = $this->Model->where("id", $id)->find();
    if(empty($user)){
      $this->redirect('index');
    }

    $prefix = Config::get("database.prefix");
    $group = Db::name('auth_group')->join("{$prefix}group_access", "{$prefix}group_access.group_id={$prefix}auth_group.id", "LEFT")->where("{$prefix}group_access.uid", $id)->find();
    $this->assign("user", $user);
    $this->assign("group", $group);
    return $this->fetch("read");
  }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function edit($id){
    $user = $this->Model->where("id", $id)->where("user_type", $this->user_type)->find();
    if(empty($user)){
      $this->redirect('index');
    }

    if($this->request->isPost()){
      $data = $this->getPostData();
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $GroupModel = new AuthGroup();
      $groups = $GroupModel->selectTypeAll($this->user_type);
      $this->assign("groups", $groups);
      $this->assign("user", $user);
      return $this->fetch("form");
    }
  }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete($id){
    $this->error("您访问的页面不存在", "index");
  }

  public function lock($id){
    if($this->request->isDelete()){
      $data = ["id" => $id, "user_type" => $this->user_type, "is_locked" => true];
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $this->error("请求方式不正确", 'index');
    }
  }

  public function unlock($id){
    if($this->request->isDelete()){
      $data = ["id" => $id, "user_type" => $this->user_type, "is_locked" => false];
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $this->error("请求方式不正确", 'index');
    }
  }

  /**
   * 修改用户类型
   * @Author zhanghong
   * @Date   2017-04-23
   * @return [type]     [description]
   */
  public function change_type($id){
    $user = $this->Model->where("id", $id)->where("user_type", $this->user_type)->find();
    if(empty($user)){
      $this->redirect('index');
    }
    
    $res = $this->Model->changeUserType($id, "member");
    if($res["status"]){
      $this->success($res["msg"], 'index');
    }else{
      $this->error($res["msg"]);
    }
  }

  // public function resort(){
  //   $param = $this->request->param();
  //   $items = $param["items"];
  //   foreach ($items as $id => $sort_num) {
  //     $sort_num = intval($sort_num);
  //     if($sort_num < 0){
  //       $sort_num = 0;
  //     }else if($sort_num >= 9999){
  //       $sort_num = 9999;
  //     }
  //     $this->Model->save(["sort" => $sort_num], ["id" => $id]);
  //   }
  //   $this->redirect('index');
  // }

  private function getPostData(){
    $atl_name = $this->request->action();
    $data = ["user_type" => $this->user_type];
    $param_name = ["id", "email", "nickname", "gender", "manager_group_id", "mobile", "url", "birthday", "password", "confirm_password", "active_score", "active_coin"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      // 编辑会员信息时不能修改密码
      if($atl_name != "create" && in_array($key, ["confirm_password", "password"])){
        continue;
      }

      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      switch ($key) {
      case "status":
      case "active_score":
      case "active_coin":
        $val = intval($val);
        break;
      case "birthday":
        if(empty($val)){
          $val = NULL;
        }
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }
}
