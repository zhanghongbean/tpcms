<?php

namespace app\admin\controller;
use think\Request;
use think\Session;
use think\Loader;
use think\Cache;
use think\Config;
use app\common\model\AuthGroup;
use app\common\model\AuthRule;
class ManagerGroupController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new AuthGroup();
  }

  public function index(){
    $param = $this->request->param();
    $res = $this->Model->mineSelect("manager", $param);
    $this->assign("param", $param);
    $this->assign("res", $res);
    return $this->fetch();
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $validate = Loader::validate('AuthGroup');
      if(!$this->Model->uniqueTitle($data)){
        $this->error("管理员组名已存在");
      }else if(!$validate->scene("manager")->check($data)){
        $msg = $validate->getError();
        $this->error($msg);
      }else{
        $this->Model->save($data);
        $this->success('新增成功', 'index');
      }
    }else{
      $MenuModel = new AuthRule();
      $all_menu_tree = $MenuModel->findWithDescendants("manager", 0, true);
      $this->assign("group_rules_id", []);
      $this->assign("group", ["status" => true]);
      $this->assign("all_menu_tree", $all_menu_tree);
      return $this->fetch("form");
    }
  }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read($id){
    $this->error("您访问的页面不存在", "index");
  }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function edit($id){
    $group = $this->Model->where("id", $id)->find();
    if(empty($group)){
      $this->redirect('index');
    }

    if($this->request->isPost()){
      $data = $this->getPostData();
      $validate = Loader::validate('AuthGroup');
      if(!$this->Model->uniqueTitle($data)){
        $this->error("管理员组名已存在");
      }else if(!$validate->scene("manager")->check($data)){
        $msg = $validate->getError();
        $this->error($msg);
      }else{
        $this->Model->save($data, ["id" => $id]);
        $this->success('编辑成功', 'index');
      }
    }else{
      $MenuModel = new AuthRule();
      $all_menu_tree = $MenuModel->findWithDescendants("manager", 0, true);
      $group_rules_id = [];
      if(isset($group["rules"])){
        $group_rules_id = explode(",", $group["rules"]);
      }
      $this->assign("group", $group);
      $this->assign("group_rules_id", $group_rules_id);
      $this->assign("all_menu_tree", $all_menu_tree);
      return $this->fetch("form");
    }
  }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete($id){
    if($this->request->isDelete()){
      $res = $this->Model->destory_item($id);
      if($res["status"]){
        $this->success($res["msg"], 'index');  
      }else{
        $this->error($res["msg"], 'index');
      }
    }else{
      $this->error("请求方式不正确", 'index');
    }
  }

  public function resort(){
    $param = $this->request->param();
    $items = $param["items"];
    foreach ($items as $id => $sort_num) {
      $sort_num = intval($sort_num);
      if($sort_num < 0){
        $sort_num = 0;
      }else if($sort_num >= 9999){
        $sort_num = 9999;
      }
      $this->Model->save(["sort" => $sort_num], ["id" => $id]);
    }
    $this->redirect('index');
  }

  public function ajax_unique_title(){
    $data = $this->getPostData();
    if($this->Model->uniqueTitle($data)){
      echo("true");
    }else{
      echo("false");
    }
  }

  private function getPostData(){
    $data = ["group_type" => "manager"];
    $param_name = ["id", "title", "status", "rules"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      if($key == "id" && empty($val)){
        continue;
      }
      
      switch ($key) {
      case "status":
        $val = intval($val);
        break;
      case "rules":
        if(is_array($val)){
          $val = implode(",", $val);
        }
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }
}
