<?php

namespace app\admin\controller;
use think\Request;
use think\Session;
use think\Loader;
use think\Cache;
use think\Config;
use app\common\model\AuthRule;
class ManagerRuleController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new AuthRule();
  }

  public function index(){
    $param = [];
    $list = $this->Model->explandWithDescendants("manager", [], 0);
    $this->assign("param", $param);
    $this->assign("list", $list);
    return $this->fetch();
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $validate = Loader::validate('AuthRule');
      if($validate->check($data)){
        $this->Model->save($data);
        //清除菜单缓存
        $this->clear_menu_cache();
        $this->success('新增成功', 'index');
      }else{
        $msg = $validate->getError();
        $this->error($msg);
      }
    }else{
      $list = $this->Model->explandWithDescendants("manager", [], 0);
      $this->assign("list", $list);
      $this->assign("rule", ["status" => true, "parent_id" => 0]);
      return $this->fetch("form");
    }
  }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read($id)
  {
    $this->redirect('index');
  }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function edit($id){
    $rule = $this->Model->where("id", $id)->find();
    if(empty($rule)){
      $this->redirect('index');
    }

    if($this->request->isPost()){
      $data = $this->getPostData();
      $validate = Loader::validate('AuthRule');
      if($validate->check($data)){
        $this->Model->save($data, ["id" => $id]);
        //清除菜单缓存
        $this->clear_menu_cache();
        $this->success('编辑成功', 'index');
      }else{
        $msg = $validate->getError();
        $this->error($msg);
      }
    }else{
      $list = $this->Model->explandWithDescendants("manager", [], 0);
      $this->assign("list", $list);
      $this->assign("rule", $rule);
      return $this->fetch("form");
    }
  }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete($id){
    if($this->request->isDelete()){
      $children_count = $this->Model->where("parent_id", $id)->count();
      if($children_count){
        $this->error("该记录有子菜单，不能删除", "index");
      }else{
        AuthRule::destroy($id);
        $this->success('删除成功', 'index');
      }
    }else{
      $this->error("请求方式不正确", 'index');
    }
  }

  public function resort(){
    $param = $this->request->param();
    $items = $param["items"];
    foreach ($items as $id => $sort_num) {
      $sort_num = intval($sort_num);
      if($sort_num < 0){
        $sort_num = 0;
      }else if($sort_num >= 9999){
        $sort_num = 9999;
      }
      $this->Model->save(["sort" => $sort_num], ["id" => $id]);
    }
    $this->redirect('index');
  }

  private function getPostData(){
    $data = ["rule_type" => "manager"];
    $param_name = ["parent_id", "title", "name", "icon", "status", "sort", "tips"];
    foreach ($param_name as $key) {
      $val = $this->request->post($key);
      
      switch ($key) {
      case "parent_id":
      case "status":
      case "sort":
        $val = intval($val);
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }

  private function clear_menu_cache(){
    $config_keys = ["menu_tree_name", "menu_expland_name"];
    foreach ($config_keys as $key => $val) {
      $key_name = Config::get("cache_keys.{$val}");
      Cache::rm($key_name);
    }
  }
}
