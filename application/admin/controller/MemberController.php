<?php

namespace app\admin\controller;
use think\Loader;
use think\Config;
use app\common\model\AuthGroup;
use app\common\model\User;
class MemberController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new User();
    $this->user_type = "member";
  }

  public function index(){
    $param = [];
    $param = $this->request->param();
    $res = $this->Model->memberSelect($param);
    // exit();
    $this->assign("param", $param);
    $this->assign("res", $res);
    return $this->fetch();
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $res = $this->Model->addUser($data);
      if($res["id"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $user = ["total_score" => 0, "active_score" => 0, "total_coin" => 0, "active_coin" => 0];
      $this->assign("user", $user);
      return $this->fetch("form");
    }
  }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read($id)
  {
    $user = $this->Model->where("id", $id)->find();
    if(empty($user)){
      $this->redirect('index');
    }
  }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function edit($id){
    $user = $this->Model->where("id", $id)->where("user_type", $this->user_type)->find();
    if(empty($user)){
      $this->redirect('index');
    }

    if($this->request->isPost()){
      $data = $this->getPostData();
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $this->assign("user", $user);
      return $this->fetch("form");
    }
  }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete($id){
    $this->error("您访问的页面不存在", "index");
  }

  public function lock($id){
    if($this->request->isDelete()){
      $data = ["id" => $id, "user_type" => $this->user_type, "is_locked" => true];
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $this->error("请求方式不正确", 'index');
    }
  }

  public function unlock($id){
    if($this->request->isDelete()){
      $data = ["id" => $id, "user_type" => $this->user_type, "is_locked" => false];
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $this->error("请求方式不正确", 'index');
    }
  }

  /**
   * 修改用户类型
   * @Author zhanghong
   * @Date   2017-04-23
   * @return [type]     [description]
   */
  public function change_type($id){
    $user = $this->Model->where("id", $id)->where("user_type", $this->user_type)->find();
    if(empty($user)){
      $this->redirect('index');
    }

    if($this->request->isPost()){
      $group_id = $this->request->post("manager_group_id");
      $res = $this->Model->changeUserType($id, "manager", $group_id);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $GroupModel = new AuthGroup();
      $groups = $GroupModel->selectTypeAll("manager");
      $this->assign("groups", $groups);
      $this->assign("user", $user);
      return $this->fetch();
    }
  }

  public function reset_password(){

  }

  // public function ajax_check_unique(){
  //   if($this->Model->isFieldValueUnique()){
  //     echo("true");
  //   }else{
  //     echo("false");
  //   }
  // }

  private function getPostData(){
    $atl_name = $this->request->action();
    $data = ["user_type" => $this->user_type];
    $param_name = ["id", "email", "nickname", "gender", "mobile", "url", "birthday", "password", "confirm_password", "active_score", "active_coin"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      // 编辑会员信息时不能修改密码
      if($atl_name != "create" && in_array($key, ["confirm_password", "password"])){
        continue;
      }

      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      if($key == "id" && empty($val)){
        continue;
      }

      switch ($key) {
      case "status":
      case "active_score":
      case "active_coin":
        $val = intval($val);
        break;
      case "birthday":
        if(empty($val)){
          $val = NULL;
        }
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }
}
