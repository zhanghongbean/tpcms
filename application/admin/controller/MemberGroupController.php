<?php

namespace app\admin\controller;
use think\Request;
use think\Session;
use think\Loader;
use think\Cache;
use think\Config;
use app\common\model\AuthGroup;
class MemberGroupController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new AuthGroup();
  }

  public function index(){
    $param = [];
    $page = $this->request->param("page");
    $param = $this->request->param();
    $res = $this->Model->mineSelect("member", $param, $page);
    $this->assign("param", $param);
    $this->assign("res", $res);
    return $this->fetch();
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $validate = Loader::validate('AuthGroup');
      if(!$this->Model->uniqueTitle($data)){
        $this->error("用户组名已存在");
      }else if(!$this->Model->checkScores($data)){
        $this->error("最大积分应该大于最小积分");
      }else if(!$validate->scene("member")->check($data)){
        $msg = $validate->getError();
        $this->error($msg);
      }else{
        $this->Model->save($data);
        $this->success('新增成功', 'index');
      }
    }else{
      $this->assign("group", ["status" => true]);
      return $this->fetch("form");
    }
  }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read($id){
    $this->redirect('index');
  }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function edit($id){
    $group = $this->Model->where("id", $id)->find();
    if(empty($group)){
      $this->redirect('index');
    }

    if($this->request->isPost()){
      $data = $this->getPostData();
      $validate = Loader::validate('AuthGroup');
      if(!$this->Model->uniqueTitle($data)){
        $this->error("用户组名已存在");
      }else if(!$this->Model->checkScores($data)){
        $this->error("最大积分应该大于最小积分");
      }else if(!$validate->scene("member")->check($data)){
        $msg = $validate->getError();
        $this->error($msg);
      }else{
        $this->Model->save($data, ["id" => $id]);
        $this->success('编辑成功', 'index');
      }
    }else{
      $this->assign("group", $group);
      return $this->fetch("form");
    }
  }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete($id){
    if($this->request->isDelete()){
      $res = $this->Model->destory_item($id);
      if($res["status"]){
        $this->success($res["msg"], 'index');  
      }else{
        $this->error($res["msg"], 'index');
      }
    }else{
      $this->error("请求方式不正确", 'index');
    }
  }

  public function resort(){
    $param = $this->request->param();
    $items = $param["items"];
    foreach ($items as $id => $sort_num) {
      $sort_num = intval($sort_num);
      if($sort_num < 0){
        $sort_num = 0;
      }else if($sort_num >= 9999){
        $sort_num = 9999;
      }
      $this->Model->save(["sort" => $sort_num], ["id" => $id]);
    }
    $this->redirect('index');
  }

  public function ajax_unique_title(){
    $data = $this->getPostData();
    if($this->Model->uniqueTitle($data)){
      echo("true");
    }else{
      echo("false");
    }
  }

  public function ajax_check_scores(){
    $data = $this->getPostData();
    if($this->Model->checkScores($data)){
      echo("true");
    }else{
      echo("false");
    }
  }

  private function getPostData(){
    $data = ["group_type" => "member"];
    $param_name = ["id", "title", "status", "min_score", "max_score"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      if($key == "id" && empty($val)){
        continue;
      }
      
      switch ($key) {
      case "status":
      case "min_score":
      case "max_score":
        $val = intval($val);
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }
}
