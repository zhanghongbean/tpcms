<?php

namespace app\admin\controller;
use app\common\model\Category;
class NewsCategoryController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new Category();
    // 在关联表里对应的类型
    $this->related_type = "News";
  }

  public function index(){
    $param = [];
    $list = $this->Model->explandWithDescendants($this->related_type, [], 0);
    $this->assign("param", $param);
    $this->assign("list", $list);
    return $this->fetch("category/index");
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $res = $this->Model->createOrUpdate($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $list = $this->Model->explandWithDescendants($this->related_type, [], 0);
      $this->assign("list", $list);
      $this->assign("category", ["status" => true, "parent_id" => 0]);
      return $this->fetch("category/form");
    }
  }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read($id)
  {
    $this->redirect("index");
  }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function edit($id){
    $category = $this->Model->where("id", $id)->where("meta_type", $this->related_type)->find();
    if(empty($category)){
      $this->redirect('index');
    }

    if($this->request->isPost()){
      $data = $this->getPostData();
      $res = $this->Model->createOrUpdate($data);
      if($res["status"]){
        $this->success($res["msg"], 'index');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $list = $this->Model->explandWithDescendants($this->related_type, [], 0);
      $this->assign("list", $list);
      $this->assign("category", $category);
      return $this->fetch("category/form");
    }
  }

  public function resort(){
    $param = $this->request->param();
    $items = $param["items"];
    foreach ($items as $id => $sort_num) {
      $sort_num = intval($sort_num);
      if($sort_num < 0){
        $sort_num = 0;
      }else if($sort_num >= 9999){
        $sort_num = 9999;
      }
      $this->Model->save(["sort_num" => $sort_num], ["id" => $id, "meta_type" => $this->related_type]);
    }
    $this->redirect('index');
  }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete($id){
    $this->error("您访问的页面不存在", "index");
  }

  private function getPostData(){
    $atl_name = $this->request->action();
    $data = ["meta_type" => $this->related_type];
    $param_name = ["id", "name", "title", "parent_id", "sort_num", "seo_title", "seo_keywords", "seo_description", "template_index", "template_list", "template_show", "is_leaf", "status"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      if($key == "id" && empty($val)){
        continue;
      }

      switch ($key) {
      case "parent_id":
      case "sort_num":
        $val = intval($val);
        break;
      case "status":
      case "is_leaf":
        if(empty($val)){
          $val = 0;
        }else{
          $val = 1;
        }
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }
}
