<?php

namespace app\admin\controller;
use app\common\model\Article;
use app\common\model\Category;
use app\common\model\Tag;
class NoteController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new Article();
    $this->Category = new Category();
    // 对象类型
    $this->type = "Note";
    // 在关联表里对应的类型
    $this->related_type = "Article";
  }

  public function index(){
    $param = [];
    $param = $this->request->param();
    $default_param = ["sort_type" => "star_desc"];
    $res = $this->Model->managerSelect(array_merge($default_param, $param, ["type" => $this->type]));

    $this->assign("param", $param);
    $this->assign("res", $res);
    return $this->fetch();
  }

  // public function create(){
  //   if($this->request->isPost()){
  //     $data = $this->getPostData();
  //     $res = $this->Model->createOrUpdate($data);
  //     if($res["status"]){
  //       $this->success($res["msg"], 'index');
  //     }else{
  //       $this->error($res["msg"]);
  //     }
  //   }else{
  //     $categories = $this->Category->explandWithDescendants($this->type, [], 0, true);
  //     $this->assign("categories", $categories);
  //     $this->assign("article", ["status" => true, "wap_cover_id" => 0, "pc_cover_id" => 0]);
  //     $this->assign("tag_names", "");
  //     return $this->fetch("form");
  //   }
  // }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  // public function read($id)
  // {
  //   $this->redirect("index");
  // }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  // public function edit($id){
  //   $article = $this->Model->where("id", $id)->find();
  //   if(empty($article)){
  //     $this->redirect('index');
  //   }

  //   if($this->request->isPost()){
  //     $data = $this->getPostData();
  //     $res = $this->Model->createOrUpdate($data);
  //     if($res["status"]){
  //       $this->success($res["msg"], 'index');
  //     }else{
  //       $this->error($res["msg"]);
  //     }
  //   }else{
  //     $categories = $this->Category->explandWithDescendants($this->type, [], 0, true);
  //     $this->assign("categories", $categories);
  //     $this->assign("article", $article);

  //     $tagModel = new Tag();
  //     $tag_names = $tagModel->searchNameByRelatedItem("Article", $id);
  //     $this->assign("tag_names", implode(",", $tag_names));
  //     return $this->fetch("form");
  //   }
  // }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete(){
    if($this->request->isDelete()){
      $id = $this->request->param("id");
      Article::destroy($id);
      $this->success("删除成功", url("index"));  
    }else{
      $this->error("您请求的方法不存在", url("index"));
    }
  }

  // private function getPostData(){
  //   $atl_name = $this->request->action();
  //   $data = [];
  //   $param_name = ["id", "category_id", "title", "seo_title", "seo_keywords", "seo_description", "description", "content", "pc_cover_id", "wap_cover_id", "status", "tag_names"];
  //   $param = $this->request->post();
  //   foreach ($param_name as $key) {
  //     if(isset($param[$key])){
  //       $val = $param[$key];
  //     }else{
  //       $val = "";
  //     }

  //     if($key == "id" && empty($val)){
  //       continue;
  //     }

  //     switch ($key) {
  //     case "sort_num":
  //       $val = intval($val);
  //       break;
  //     case "status":
  //       if(empty($val)){
  //         $val = 0;
  //       }else{
  //         $val = 1;
  //       }
  //       break;
  //     default:
  //       $val = trim($val);  
  //     }
  //     $data[$key] = $val;
  //   }
  //   return $data;
  // }
}
