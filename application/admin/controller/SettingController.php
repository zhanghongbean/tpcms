<?php

namespace app\admin\controller;
use think\Session;
use think\Cache;
use app\common\model\Options;
class SettingController extends BaseController{
  // 全站设置
  public function site(){
    $Model = new Options();
    $option_name = "site_options";
    if($this->request->isPost()){
      $params = $this->request->post();
      $data = $params["options"];
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
       
      $Model->saveSetting($option_name, $data);
      Session::flash("notice", "更新成功");
      $this->redirect($this->request->action());
    }

    $notice = Session::pull('notice');
    $this->assign("notice", $notice);

    $options = $Model->readSetting($option_name);
  
    $this->assign("options", $options);
    return $this->fetch();
  }

  // smtp设置
  public function smtp(){
    $Model = new Options();
    $option_name = "smtp_options";
    if($this->request->isPost()){
      $params = $this->request->post();
      $data = $params["options"];
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      if(!isset($data["smtp_open"])){
        // 不启用smtp
        $data["smtp_open"] = "0";
      }
      $Model->saveSetting($option_name, $data);
      Session::flash("notice", "更新成功");
      $this->redirect($this->request->action());
    }

    $notice = Session::pull('notice');;
    $this->assign("notice", $notice);

    $secure_list = ["ssl" => "普通连接方式", "tls" => "TLS连接方式"];
    $this->assign("secure_list", $secure_list);

    $options = $Model->readSetting($option_name);

    if(isset($options["smtp_secure"])){
      $smtp_secure = $options["smtp_secure"];
    }else{
      $smtp_secure = "";
    }

    $this->assign("options", $options);
    $this->assign("smtp_secure", $smtp_secure);
    return $this->fetch();
  }  

  // url model设置
  public function url(){
    $Model = new Options();
    $option_name = "url_options";
    if($this->request->isPost()){
      $params = $this->request->post();
      $data = $params["options"];
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      $Model->saveSetting($option_name, $data);
      Session::flash("notice", "更新成功");
      $this->redirect($this->request->action());
    }

    $notice = Session::pull('notice');;
    $this->assign("notice", $notice);

    $model_list = ["0" => "普通模式", "1" => "PATHINFO模式", "2" => "REWRITE模式", "3" => "兼容模式"];
    $this->assign("model_list", $model_list);

    $options = $Model->readSetting($option_name);
    if(isset($options["url_model"])){
      $url_model = $options["url_model"];
    }else{
      $url_model = "2";
    }

    $this->assign("options", $options);
    $this->assign("url_model", $url_model);
    return $this->fetch();
  }

  // 激活邮件
  public function active(){
    $Model = new Options();
    $option_name = "active_options";
    if($this->request->isPost()){
      $params = $this->request->post();
      $data = $params["options"];
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      if(!isset($data["email_active"])){
        // 不启用email active
        $data["email_active"] = "0";
      }
      $Model->saveSetting($option_name, $data);
      Session::flash("notice", "更新成功");
      $this->redirect($this->request->action());
    }

    $notice = Session::pull('notice');;
    $this->assign("notice", $notice);

    $options = $Model->readSetting($option_name);

    $this->assign("options", $options);
    return $this->fetch();
  }

  // 第三方授权登录
  public function oauth(){
    $Model = new Options();
    $option_name = "oauth_options";
    if($this->request->isPost()){
      $params = $this->request->post();
      $data = $params["options"];
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      $Model->saveSetting($option_name, $data);
      Session::flash("notice", "更新成功");
      $this->redirect($this->request->action());
    }

    $notice = Session::pull('notice');;
    $this->assign("notice", $notice);

    $options = $Model->readSetting($option_name);
    $this->assign("options", $options);
    return $this->fetch();
  }
}
