<?php
// +-------------+--------------+------+-----+---------+----------------+
// | Field       | Type         | Null | Key | Default | Extra          |
// +-------------+--------------+------+-----+---------+----------------+
// | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
// | module      | varchar(30)  | NO   |     |         |                |
// | controller  | varchar(30)  | NO   |     |         |                |
// | action      | varchar(30)  | NO   |     |         |                |
// | user_id     | int(11)      | NO   |     | 0       |                |
// | user_name   | varchar(30)  | NO   |     |         |                |
// | querystring | varchar(255) | NO   |     |         |                |
// | ip          | int(11)      | NO   |     | 0       |                |
// | create_time | int(11)      | NO   |     | 0       |                |
// | method      | varchar(10)  | YES  |     |         |                |
// | param       | text         | YES  |     | NULL    |                |
// +-------------+--------------+------+-----+---------+----------------+
namespace app\admin\model;

use think\Model;
use think\Request;
class AdminLog extends Model{
  protected $updateTime = null;

  /*
  记录管理员操作日志
   */
  public function add($manager){
    $request = Request::instance();
    $ctrl_name = $request->controller();
    $act_name = $request->action();
    if(in_array($ctrl_name, array("Index")) || in_array($act_name, ["index", "read", "show", "detail"])){
      return true;
    }

    if(empty($manager)){
      $this->user_id = 0;
      $this->user_name = "";
    }else{
      $this->user_id = $manager["id"];
      $this->user_name = $manager["nickname"];
    }
    
    $this->module = $request->module();
    $this->controller = $ctrl_name;
    $this->action = $act_name;
    $this->method = $request->method();
    $param = $request->param();
    if(empty($param)){
      $param = [];
    }
    $this->param = json_encode($param);
    $query_str = $request->query();
    if(empty($query_str)){
      $query_str = "";
    }
    $this->querystring = $query_str;
    $this->ip = ip2long($request->ip());
    // $this->save();
  }

  /*
  protected $auto = ['module', 'user_id', 'user_name', 'controller', 'action', 'querystring', 'ip'];

  protected function setUserIdAttr(){
    return Request::instance()->session('user_id');
  }

  protected function setUserNameAttr(){
    return Request::instance()->session('user_name');
  }

  protected function setModuleAttr(){
    return request()->module();
  }

  protected function setControllerAttr(){
    return request()->controller();
  }

  protected function setActionAttr(){
    return request()->action();
  }

  protected function setQuerystringAttr(){
    return request()->query() ? request()->query() : '';
  }

  protected function setIpAttr(){
    return ip2long(request()->ip());
  }
  */
}
