<?php
namespace app\common\model;

use think\Model;

class AdminBase extends Model{
  protected $auto = ['editor_id'];

  protected function setEditorIdAttr(){
    return login_manager_id();
  }
}