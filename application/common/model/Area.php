<?php
// +------------+-------------+------+-----+---------+----------------+
// | Field      | Type        | Null | Key | Default | Extra          |
// +------------+-------------+------+-----+---------+----------------+
// | id         | int(11)     | NO   | PRI | NULL    | auto_increment |
// | taobao_id  | int(11)     | YES  |     | 0       |                |
// | name       | varchar(30) | YES  |     |         |                |
// | parent_id  | int(11)     | YES  | MUL | 0       |                |
// | pinyin     | varchar(10) | YES  |     |         |                |
// | zipcode    | varchar(8)  | YES  |     |         |                |
// | updater_id | int(11)     | YES  |     | 0       |                |
// | created_at | datetime    | YES  |     | NULL    |                |
// | updated_at | datetime    | YES  |     | NULL    |                |
// +------------+-------------+------+-----+---------+----------------+
namespace app\common\model;
use app\common\model\Base;
use think\Request;
class Area extends Base{
  public function children($parent_id){
    return $this->field("id,name")->where("parent_id", $parent_id)->select();
  }
}