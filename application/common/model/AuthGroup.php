<?php
// +-------------+--------------+------+-----+---------+----------------+
// | Field       | Type         | Null | Key | Default | Extra          |
// +-------------+--------------+------+-----+---------+----------------+
// | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
// | name        | varchar(30)  | NO   |     |         |                |
// | title       | varchar(30)  | NO   |     |         |                |
// | rules       | varchar(200) | NO   |     |         |                |
// | create_time | int(11)      | NO   |     | 0       |                |
// | update_time | int(11)      | NO   |     | 0       |                |
// | status      | tinyint(1)   | YES  |     | 1       |                |
// | group_type  | varchar(30)  | YES  |     |         |                |
// | editor_id   | int(11)      | YES  |     | 0       |                |
// | sort        | int(11)      | YES  |     | 0       |                |
// | min_score   | int(11)      | YES  |     | 0       |                |
// | max_score   | int(11)      | YES  |     | 0       |                |
// +-------------+--------------+------+-----+---------+----------------+
namespace app\common\model;
use app\common\model\AdminBase;
use think\Db;
use think\Config;
class AuthGroup extends AdminBase{
  /**
   * 查询访问
   * @Author zhanghong
   * @Date   2017-01-06
   * @param  string     $group_type  群组类型
   * @param  array      $param      查询参数
   * @param  integer    $page_rows  每页显示的记录数量
   * @return Paginate               
   */
  public function mineSelect($group_type, $param, $page_rows = 15){
    $this->where("group_type", $group_type);

    $config = [];
    foreach ($param as $key => $value) {
      if(!is_array($value)){
        $value = trim($value);
      }
      switch ($key) {
      case "keyword":
        if(empty($value)){
          continue;
        }else{
          $keyword = "%{$value}%";
        }
        $this->where("title", "like", $keyword);
        break;
      }
    }
    $config["query"] = $param;

    // order string
    $order_str = "sort, id DESC";
    // $order_str = "tp_auth_group.sort, tp_auth_group.id desc";
    if(isset($param["order"])){
      // switch ($param["order"]) {
      // default:
      //   $order_str = "{$prefix}rule_group.sort, {$prefix}rule_group.id desc";
      //   break;
      // }
    }

    $this->order($order_str);
    // 分页参数名是 page ，paginate方法内有获取到该值的方法
    $page = 1;
    if(isset($param["page"])){
      $page = intval($param["page"]);
    }
    $config["page"] = $page;
    $paginate = $this->paginate($page_rows, false, $config);
    return $paginate;
  }

  /**
   * 单个类型的所有auth_group
   * @Author zhanghong
   * @Date   2017-01-06
   * @param  string     $group_type  群组类型
   * @param  boolean    $only_active 是否只返回active记录
   * @return array                   AuthGroup List
   */
  public function selectTypeAll($group_type, $only_active = true){
    $this->where("group_type", $group_type);

    if($only_active){
      $this->where("status", true);
    }

    $this->order("sort, id DESC");
    return $this->select();
  }

  /**
   * 用户所在组
   * @Author zhanghong
   * @Date   2017-01-01
   * @param  integer     $user_id 用户ID
   * @return array                AuthGroup
   */
  public function userGroup($user_id){
    $prefix = Config::get("database.prefix");
    $group = $this->alias('g')->join("{$prefix}group_access a", "g.id=a.group_id")
                  ->where("a.uid", $user_id)->find();
    return $group;
  }

  /**
   * 验证输入的积分区间是否正确
   * @Author zhanghong
   * @Date   2017-01-05
   * @param  array      $data  输入数据
   * @return boolean           
   */
  public function checkScores($data){
    if($data["min_score"] >= $data["max_score"]){
      return false;
    }
    return true;
  }


  public function memberGroupItem($active_score){
    $active_score = intval($active_score);
    $item = $this->where("group_type", "member")
                 ->where("min_score", "<=", $active_score)
                 ->where("max_score", ">=", $active_score)
                 ->order("sort, min_score ASC")
                 ->find();
    return $item;
  }

  /**
   * 删除记录
   * @Author zhanghong
   * @Date   2017-01-06
   * @param  integer    $id 主键
   * @return array
   */
  public function destory_item($id){
    $res = array("status" => false, "msg" => "删除失败");
    $access_count = Db::name('group_access')->where("group_id", $id)->count();
    if($access_count){
      $res["msg"] = "不能删除有用户的组";
    }else{
      AuthGroup::destroy($id);
      $res = array("status" => true, "msg" => "删除成功");
    }
    return $res;
  }

  /**
   * 验证输入的AuthGroup Title是否唯一（因为Validate的带条件查询有问题，不能使用）
   * @Author zhanghong
   * @Date   2017-01-05
   * @param  array      $data  输入数据
   * @return boolean           
   */
  public function uniqueTitle($data){
    $this->where("status", 1)
         ->where("title", $data["title"])
         ->where("group_type", $data["group_type"]);
    if(!empty($data["id"])){
      $this->where("id", "<>", $data["id"]);
    }
    if($this->count()){
      return false;
    }
    return true;
  }
}