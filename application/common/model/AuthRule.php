<?php
// +-------------+--------------+------+-----+---------+----------------+
// | Field       | Type         | Null | Key | Default | Extra          |
// +-------------+--------------+------+-----+---------+----------------+
// | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
// | name        | varchar(50)  | YES  |     |         |                |
// | title       | varchar(30)  | YES  |     |         |                |
// | type        | tinyint(1)   | YES  |     | 1       |                |
// | status      | tinyint(1)   | YES  |     | 1       |                |
// | condition   | varchar(100) | YES  |     |         |                |
// | create_time | int(11)      | NO   |     | 0       |                |
// | update_time | int(11)      | NO   |     | 0       |                |
// | parent_id   | int(11)      | YES  |     | 0       |                |
// | icon        | varchar(50)  | YES  |     |         |                |
// | islink      | tinyint(1)   | NO   |     | 1       |                |
// | sort        | int(11)      | YES  |     | 0       |                |
// | tips        | text         | YES  |     | NULL    |                |
// | editor_id   | int(11)      | YES  |     | 0       |                |
// | rule_type   | varchar(20)  | YES  |     |         |                |
// +-------------+--------------+------+-----+---------+----------------+
namespace app\common\model;
use app\common\model\AdminBase;
class AuthRule extends AdminBase{

  /**
   * 树状结构菜单
   * @Author zhanghong
   * @Date   2017-01-01
   * @param  string     $rule_type 菜单类型
   * @param  integer    $parent_id 根结点ID
   * @param  boolean    $skip_hide 是否忽略隐藏结点
   * @param  integer    $level     结点深度
   * @return array                 Tree
   */
  public function findWithDescendants($rule_type, $parent_id = 0, $skip_hide = true, $level = 0){
    $this->where("parent_id", $parent_id)->where("rule_type", $rule_type);
    if($skip_hide){
      $this->where("status", 1);
    }
    $children = $this->order("sort, id")->select();
    if(!empty($children)){
      //如果有子类
      foreach ($children as $key => &$child) {
        $child["deep_level"] = $level;
        $child["children"] = $this->findWithDescendants($rule_type, $child["id"], $skip_hide, $level+1);
      }
    }

    return $children;
  }

  /**
   * 一维数组结构菜单
   * @Author zhanghong
   * @Date   2017-01-01
   * @param  string     $rule_type 菜单类型
   * @param  array      $list      保存菜单的数组
   * @param  integer    $parent_id 根结点ID
   * @param  array      $ancestor_ids 所有祖先结点ID
   * @param  string     $prefix       显示名前辍
   * @return array                    Rule集合
   */
  public function explandWithDescendants($rule_type, $list, $parent_id = 0, $ancestor_ids = [], $prefix=""){
    $children = $this->where("parent_id", $parent_id)->where("rule_type", $rule_type)->order("sort, id")->select();
    if(!empty($children)){
      //如果有子类
      $level = count($ancestor_ids);
      foreach ($children as $key => $child) {

        $child["deep_level"] = $level;
        $child["shown_title"] = $prefix.$child["title"];
        $list[] = $child;
        
        $sub_ancestor_ids = $ancestor_ids;
        array_push($sub_ancestor_ids, $child["id"]);
        //所有祖先结点和自己的ID列表
        $child["with_ancestor_ids"] = $sub_ancestor_ids;
        if($level == 0){
          //第二级菜单
          $sub_prefix = "┗━";
        }else if($level > 0){
          //第二级以后的菜单
          $sub_prefix = '&nbsp;&nbsp;&nbsp;&nbsp;'.$prefix;
        }
        $list = $this->explandWithDescendants($rule_type, $list, $child["id"], $sub_ancestor_ids, $sub_prefix);
      }
    }
    return $list;
  }

}