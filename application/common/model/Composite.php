<?php
namespace app\common\model;
use think\Config;
use think\Loader;
// use app\common\validate\User;
use app\common\model\Base;
use app\common\model\AuthGroup;
use app\common\model\ScoreLog;
use app\common\model\GroupAccess;
use think\Request;
class Composite extends Base{
  
  
public function generateRececertImage($slogan){  
    //$moban_list = list_field_moban();
    //$thismoban=$moban_list[$mobanid];  
    $nodes = array();
    //背景图
    $nodes[0] = array('type' => 'image',
                      'image' => ROOT_PATH.'/public/static/admin/images/cert_background.jpg',
                      'locate' => array(0 => 0,1 =>0),//(0 =>右magrin,1=>上magrin)
                      'width' => 640,
                      'height' => 1000
     );
    //右上角图标
    $nodes[1] = array('type' => 'image',
                      'image' => ROOT_PATH.'/public/static/admin/images/certlogo.png',
                      'locate' => array(0 => 0,1 =>0),
                      'width' => 640,
                      'height' => 1000
     );
    //完赛信息背景图
    $nodes[2] = array('type' => 'image',
                      'image' => ROOT_PATH.'/public/static/admin/images/certinfobg.png',
                      'locate' => array(0 => 0,1 =>0),
                      'width' => 640,
                      'height' => 1000
     );    
    //所选文字图
    $nodes[3] = array('type' => 'image',
                      'image' => ROOT_PATH.'/public/static/admin/images/'.$slogan,
                      'locate' => array(0 => 0,1 =>0),
                      'width' => 640,
                      'height' => 1000
     );    //$infotleft=$thismoban["x"];
    //$infotop=$thismoban["y"];
    $nodes[4] = array('type' => 'text',
                      'text' => "姓名：0000000",
                      'locate' => array(0 => 465,1 =>680),
                      'width' => "300",
                      'height' => "57",
                      'align' => "center",
                      'font' => "Microsoft YaHei",
                      'size' =>"48",
                      'color' => "#FFFFFF",
     );
    $nodes[5] = array('type' => 'text',
                      'text' => "里程：000000",
                      'locate' => array(0 => 460,1 =>760),
                      'width' => "300",
                      'height' => "57",
                      'align' => "center",
                      'font' => "Microsoft YaHei",
                      'size' =>"24",
                      'color' => "#FFFFFF",
     );
    $nodes[6] = array('type' => 'text',
                      'text' => "用时：0.0.0",
                      'locate' => array(0 => 455,1 =>820),
                      'width' => "300",
                      'height' => "42",
                      'align' => "center",
                      'font' => "Microsoft YaHei",
                      'size' =>"20",
                      'color' => "#F79C55",
     );
    $nodes[7] = array('type' => 'text',
                      'text' => "排名:00",
                      'locate' => array(0 => 455,1 =>880),
                      'width' => "300",
                      'height' => "42",
                      'align' => "center",
                      'font' => "Microsoft YaHei",
                      'size' =>"20",
                      'color' => "#F79C55",
     );
    // $source_path = APP_ROOT.$data['crop_path'];   
      
    $source_path = ROOT_PATH.'/public/static/admin/images/beijing_image.jpg';
    $id=8080;
    $y =  intval($id/5000);
    $target_path = ROOT_PATH.'/public/composite/'.$y.'/'.$id.'.png';
    
    if(!is_dir(dirname($target_path))){
      mkdir(dirname($target_path),0777,true);
    }
      
    $MergeModel = new MergeImage();
    $MergeModel->merge_images($source_path, $target_path, $nodes); 
    $res = array('res' => true,'msg' => '成功','id' => $id,'targetpath' =>str_replace(ROOT_PATH,"",$target_path) );
    return $res;
  }
  
}