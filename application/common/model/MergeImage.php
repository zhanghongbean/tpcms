<?php

namespace app\common\model;
class MergeImage{
  public function merge_images($source_img, $target_img, $merge_nodes)  {
    $image = \think\Image::open($source_img);
    foreach ($merge_nodes as $node)    {
      if (empty($node["alpha"]))      {
        $alpha = 100;
      }
      if (!empty($node["text"]))      {
        if (empty($node["offset"]))        {
          $offset = 0;
        } else{
          $offset = $node["offset"];
        }
        if (empty($node["angle"])){
          $angle = 0;
        } else{
          $angle = $node["angle"];
        }
        $font = $this -> get_font_file($node["font"]);
        //$image->box_text($node["text"], $font, $node["size"], $node["width"], $node["height"], $node["align"], $node["color"], $node["locate"], $offset, $angle);
        //资源检测
        if (!is_file($font))
          E("不存在的字体文件：{$font}");
        // 自定义文字坐标
        if (is_array($node["locate"])){
          list($x, $y) = $node["locate"];
        } else{
          E('不支持的文字位置类型');
        }
        //判断文字长度
        $wrap_contents =$this -> wrapContent($node["text"], $font, $node["size"], $node["width"], $angle);
        $line_x = $x;
        $line_y = $y;
        foreach ($wrap_contents as $key => $line){
          switch ($node["align"]) {
            case "center":
            $line_x = ($node["width"] - $line["width"]) / 2 + $x;
            break;
            case "right":
            $line_x = $node["width"] + $x - $line["width"];
            break;
            default:
            $line_x = $x;
            break;
          }
          $line_locate = array($line_x, $line_y);
          $image->text($line["content"], $font, $node["size"], $node["color"], $line_locate,
            $offset, $angle);
          $line_y = $line_y + intval($line["height"] * 1.2);
        }
      } else
      if (!empty($node["image"])) {
        $image->water($node["image"], $node["locate"], $alpha);
      }
    }
    $image->save($target_img);
  }

  private function get_font_file($str) {
    $name = "";
    switch ($str) {
      case "Arial":
        $name = "arial.ttf";
        break;
      case "msyhbold":
        $name = "msyhbold.ttf";
        break;
      case "Lantinghei":
      case "Lantinghei TC Extralight":
        $name = "fzlth.ttf";
        break;
      case "zhanku_happy":
        $name = "zhanku_happy.ttf";
        break;
      case "gaoduanhei":
        $name = "gaoduanhei.ttf";
        break;
      case "851tegaki_zatsu":
        $name = "851tegaki_zatsu.ttf";
        break;
      case "heiti":
        $name = "SimHei.ttf";
        break;
      case "songti":
        $name = "songti.ttf";
        break;
      case "siyuan":
        $name = "SYHTBold.otf";
        break;
      case "sybold":
        $name = "SYBold.ttf";
        break;
      case "syheavy":
        $name = "SYHTHeavy.otf";
        break;
      case "likes_4_0":
        $name = "likes_4_0.ttf";
        break;
      case "happyzcool":
        $name = "happyzcool.ttf";
        break;
      case "雅黑":
      case "微软雅黑":
      case "Microsoft YaHei":
      case "YaHei":
        $name = "yahei.ttf";
        break;
      default:
        $name = "msyh.ttf";
    }
    return ROOT_PATH . '/public/fonts/' . $name;
    
  }

  /**
   * 功能 ： 控制合成文字换行
   * @param  string  text 添加的文字
   * @param  string  font 字体路径
   * @param  integer  size 字号
   * @param  integer  width 合成文字宽度
   * @param  integer  angle 文字倾斜角度
   */
  private function wrapContent($text, $font, $size, $width, $angle){
    $lines = array();
    $r_idx = 0;
    $line_content = "";

    // 将字符串拆分成一个个单字 保存到数组 letter 中
    for ($i = 0; $i < mb_strlen($text); $i++) {
      $letter[] = mb_substr($text, $i, 1, 'utf-8');
    }

    foreach ($letter as $l) {
      $teststr = $line_content . " " . $l;
      $testbox = imagettfbbox($size, $angle, $font, $teststr);

      // 判断拼接后的字符串是否超过预设的宽度
      $line_width = $testbox[2] - $testbox[0];
      $line_height = $testbox[1] - $testbox[7];

      if ($line_width >= $width) {
        $r_idx++;
        $line_content = $l;
      } else {
        $line_content .= $l;
      }

      $lines[$r_idx] = array(
        "content" => $line_content,
        "width" => $line_width,
        "height" => $line_height);
    }
    return $lines;
  }

}
