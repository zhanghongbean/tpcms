<?php
// +-----------------+--------------+------+-----+---------+----------------+
// | Field           | Type         | Null | Key | Default | Extra          |
// +-----------------+--------------+------+-----+---------+----------------+
// | id              | int(11)      | NO   | PRI | NULL    | auto_increment |
// | category_id     | int(11)      | YES  |     | 0       |                |
// | name            | varchar(50)  | YES  |     |         |                |
// | title           | varchar(255) | YES  |     |         |                |
// | sort_num        | int(11)      | YES  |     | 0       |                |
// | seo_title       | varchar(255) | YES  |     |         |                |
// | seo_keywords    | varchar(255) | YES  |     |         |                |
// | seo_description | varchar(255) | YES  |     |         |                |
// | description     | varchar(255) | YES  |     |         |                |
// | content         | text         | YES  |     | NULL    |                |
// | pc_cover_id     | int(11)      | YES  |     | 0       |                |
// | wap_cover_id    | int(11)      | YES  |     | 0       |                |
// | status          | tinyint(1)   | YES  |     | 0       |                |
// | user_id         | int(11)      | YES  |     | 0       |                |
// | create_time     | int(11)      | YES  |     | 0       |                |
// | update_time     | int(11)      | YES  |     | 0       |                |
// | is_origin       | tinyint(1)   | YES  |     | 0       |                |
// | origin_author   | varchar(100) | YES  |     |         |                |
// | star_up         | int(11)      | YES  |     | 0       |                |
// | star_down       | int(11)      | YES  |     | 0       |                |
// +-----------------+--------------+------+-----+---------+----------------+
namespace app\common\model;
use think\Loader;
use think\Config;
use think\Db;
use app\common\model\Tag;
use app\common\model\StarLog;
class News extends Base{
  
}