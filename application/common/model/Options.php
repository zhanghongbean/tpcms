<?php
// +-------------+-------------+------+-----+---------+----------------+
// | Field       | Type        | Null | Key | Default | Extra          |
// +-------------+-------------+------+-----+---------+----------------+
// | id          | int(11)     | NO   | PRI | NULL    | auto_increment |
// | name        | varchar(60) | NO   |     |         |                |
// | value       | text        | NO   |     | NULL    |                |
// | autoload    | tinyint(1)  | NO   |     | 1       |                |
// | create_time | int(11)     | NO   |     | 0       |                |
// | update_time | int(11)     | NO   |     | 0       |                |
// | editor_id   | int(11)     | YES  |     | 0       |                |
// +-------------+-------------+------+-----+---------+----------------+
namespace app\common\model;
use think\Cache;
use think\Config;
use app\common\model\AdminBase;
class Options extends AdminBase{
  /**
   * 保存Option设置
   * @Author zhanghong
   * @Date   2017-05-16
   * @param  string     $name [description]
   * @param  array     $data [description]
   * @return boolean
   */
  public function saveSetting($name, $data){
    $map = ["name" => $name];
    $opt = $this->where($map)->find();
    if(is_array($data)){
      $json_str = json_encode($data);
      if(empty($opt)){
        $this->data(["name" => $name, "value" => $json_str]);
        $this->save();
      }else{
        $this->where(["id" => $opt["id"]])->update(["value" => $json_str]);
      }
      // 删除旧缓存
      Cache::rm("option_{$name}");
    }
    
    return true;
  }

  /**
   * 读取Option值
   * @Author zhanghong
   * @Date   2017-05-16
   * @param  string     $name option name
   * @return array
   */
  public function readSetting($name){
    $key_name = "option_{$name}";
    $val = Cache::get($key_name);
    if(!empty($val)){
      return $val;
    }

    $map = ["name" => $name];
    $default_val = [];

    if(empty($name)){
      return $default_val;
    }

    $opt = $this->where($map)->find();
    
    if(empty($opt)){
      return $default_val;
    }

    $val = json_decode($opt["value"], true);
    // 缓存过期时间在config.php文件里设置
    Cache::set($key_name, $val);
    return $val;
  }

  /**
   * 读取全站所有设置数据
   * @Author zhanghong
   * @Date   2017-05-16
   * @return [type]     [description]
   */
  public function readAndSetConfig(){
    $list = [];
    $options = $this->field(["id", "name"])->select();
    foreach ($options as $key => $opt) {
      $opt_name = $opt["name"];
      $val = $this->readSetting($opt_name);
      $list[$opt_name] = $val;
      Config::set($opt_name, $val);
    }
    return $list;
  }

  /**
   * 读取站点Host
   * @Author zhanghong
   * @Date   2017-05-16
   * @return [type]     [description]
   */
  public function getSiteHost(){
    $options = $this->readSetting("site_options");
    if(!empty($options) && isset($options["site_host"])){
      $site_root = $options["site_host"];
    }else{
      $site_root = Config::get("site_host");
    }
    return $site_root;
  }
}