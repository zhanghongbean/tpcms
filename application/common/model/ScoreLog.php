<?php
// +--------------+--------------+------+-----+---------+----------------+
// | Field        | Type         | Null | Key | Default | Extra          |
// +--------------+--------------+------+-----+---------+----------------+
// | id           | int(11)      | NO   | PRI | NULL    | auto_increment |
// | user_id      | int(11)      | YES  |     | 0       |                |
// | total_score  | int(11)      | YES  |     | 0       |                |
// | active_score | int(11)      | YES  |     | 0       |                |
// | inc_score    | int(11)      | YES  |     | 0       |                |
// | total_coin   | int(11)      | YES  |     | 0       |                |
// | active_coin  | int(11)      | YES  |     | 0       |                |
// | inc_coin     | int(11)      | YES  |     | 0       |                |
// | comment      | varchar(100) | YES  |     |         |                |
// | related_type | varchar(30)  | YES  |     |         |                |
// | related_id   | int(11)      | YES  |     | 0       |                |
// | editor_id    | int(11)      | YES  |     | 0       |                |
// | create_time  | int(11)      | YES  |     | 0       |                |
// | update_time  | int(11)      | YES  |     | 0       |                |
// +--------------+--------------+------+-----+---------+----------------+
namespace app\common\model;
use app\common\model\Base;
class ScoreLog extends Base{
  protected $auto = ['editor_id'];

  protected function setEditorIdAttr(){
    return login_user_id();
  }

  function addLog($data){
    $this->allowField(true)->data($data);
    $this->save();
    return $this->id;
  }
}