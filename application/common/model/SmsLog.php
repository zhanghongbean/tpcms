<?php
// +-------------+--------------+------+-----+---------+----------------+
// | Field       | Type         | Null | Key | Default | Extra          |
// +-------------+--------------+------+-----+---------+----------------+
// | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
// | user_id     | int(11)      | YES  |     | 0       |                |
// | mobile      | varchar(11)  | YES  |     |         |                |
// | sms_type    | varchar(10)  | YES  |     |         |                |
// | content     | varchar(255) | YES  |     |         |                |
// | code        | varchar(6)   | YES  |     |         |                |
// | send_status | varchar(10)  | YES  |     | padding |                |
// | sms_no      | varchar(30)  | YES  |     |         |                |
// | expire_time | int(11)      | YES  |     | 0       |                |
// | create_time | int(11)      | YES  |     | 0       |                |
// | update_time | int(11)      | YES  |     | 0       |                |
// | client_ip   | bigint(20)   | YES  |     | 0       |                |
// +-------------+--------------+------+-----+---------+----------------+
namespace app\common\model;
use app\common\model\Base;
use think\Request;
use think\Config;
use think\Db;
class SmsLog extends Base{
  protected $auto = ['user_id'];

  /**
   * 发送注册验证短信
   * @Author zhanghong
   * @Date   2017-01-13
   * @param  string     $mobile mobile
   * @return array
   */
  public function sendRegistMsg($mobile){
    $res = ["status" => false, "msg" => "发送注册验证短信失败"];
    $sms_type = "register";

    $user = Db::name("user")->where("mobile", $mobile)->find();
    if($user){
      $res["msg"] = "该手机号码已注册";
      return $res;
    }

    $current_time = time();

    $map = ["mobile" => $mobile, "send_status" => "successed"];
    $log = $this->where($map)->where("expire_time", "GT", $current_time)->find();
    if($log){
      $res = ["status" => true, "msg" => "验证短信已发送", "data" => ["code" => $log["code"]]];
      return $res;
    }

    $request = Request::instance();
    $int_ip = ip2long($request->ip());
    $map = ["client_ip" => $int_ip, "send_status" => "successed"];
    $send_count = $this->where($map)->where("expire_time", "GT", $current_time)->count();
    if($send_count){
      $res["msg"] = "同一IP地址请求太频繁，请稍后再试";
      return $res;
    }

    $cfg_sms = Config::get("ztsms");
    if(isset($cfg_sms["sign"])){
      $sign = trim($cfg_sms["sign"]);
    }else{
      $sign = "";
    }
    $this->client_ip = $int_ip;
    $this->mobile = $mobile;
    $this->sms_type = $sms_type;
    $code = $this->generateCode();
    $this->code = $code;
    $this->content = "尊敬的用户，您的验证码：{$code}，如非本人操作，请忽略本短信！".$sign;
    $this->save();
    $send_res = $this->sendByZTSms($this->id);
    // test
    // $send_res = ["status" => true];
    if($send_res["status"]){
      $res = ["status" => true, "msg" => "注册验证短信发送成功", "data" => ["code" => $code]];
    }else{
      $res["msg"] = $send_res["msg"];
    }
    return $res;
  }

  /**
   * 找回密码手机验证
   * @Author zhanghong
   * @Date   2017-01-13
   * @param  string     $mobile mobile
   * @return array
   */
  public function sendRetrieveMsg($mobile){
    $res = ["status" => false, "msg" => "发送找回密码验证短信失败"];
    $sms_type = "retrieve";

    $user = Db::name("user")->where("mobile", $mobile)->find();
    if(empty($user)){
      $res["msg"] = "该手机号码未注册";
      return $res;
    }

    $current_time = time();
    $map = ["mobile" => $mobile, "send_status" => "successed"];
    $log = $this->where($map)->where("expire_time", "GT", $current_time)->find();

    if($log){
      $res = ["status" => true, "msg" => "验证短信已发送", "data" => ["code" => $log["code"]]];
      return $res;
    }

    $request = Request::instance();
    $int_ip = ip2long($request->ip());
    $map = ["client_ip" => $int_ip, "send_status" => "successed"];
    $send_count = $this->where($map)->where("expire_time", "GT", $current_time)->count();
    if($send_count){
      $res["msg"] = "同一IP地址请求太频繁，请稍后再试";
      return $res;
    }

    $cfg_sms = Config::get("ztsms");
    if(isset($cfg_sms["sign"])){
      $sign = trim($cfg_sms["sign"]);
    }else{
      $sign = "";
    }
    $this->client_ip = $int_ip;
    $this->mobile = $mobile;
    $this->sms_type = $sms_type;
    $code = $this->generateCode();
    $this->code = $code;
    $this->content = "尊敬的用户，您的验证码：{$code}，如非本人操作，请忽略本短信！".$sign;
    $this->save();
    $send_res = $this->sendByZTSms($this->id);
    // test
    // $send_res = ["status" => true];
    if($send_res["status"]){
      $res = ["status" => true, "msg" => "找回密码验证短信发送成功", "data" => ["code" => $code]];
    }else{
      $res["msg"] = $send_res["msg"];
    }
    return $res;
  }


  /**
   * 验证用户输入的验证码是否正确
   * @Author zhanghong
   * @Date   2017-04-23
   * @param  string     $sms_type 短信类型
   * @param  string     $mobile   用户手机号码
   * @param  string     $code     验证码
   * @return array
   */
  public function validSendCode($sms_type, $mobile, $code){
    $res = array("status" => false, "msg" => "验证码校验失败");
    if(empty($sms_type)){
      $res["msg"] = "短信类型不能为空";
      return $res;
    }
    if(empty($mobile)){
      $res["msg"] = "用户手机号码不能为空";
      return $res;
    }
    if(empty($code)){
      $res["msg"] = "验证码不能为空";
      return $res;
    }
    $current_time = time();
    $count = $this->where("mobile", $mobile)->where("sms_type", $sms_type)->where("code", $code)->where("expire_time", "egt", $current_time)->count();

    if($count){
      $res = ["status" => true, "msg" => "输入的验证码正确"];
    }else{
      $res["msg"] = "输入的验证码不正确或已过期";
    }
    return $res; 
  }

  /**
   * 发送短信Log
   * @Author zhanghong
   * @Date   2017-01-13
   * @param  integer    $id 短信Log ID
   * @return array
   */
  public function sendByZTSms($id){
    $res = ["status" => false, "msg" => "短信发送失败"];
    $log = $this->where("id", $id)->find();
    if(empty($log)){
      $res["msg"] = "短信记录不存在";
      return $res;
    }else if($log["send_status"] != "padding"){
      $res["msg"] = "短信记录状态不正确";
      return $res;
    }

    $cfg_sms = Config::get("ztsms");
    if(isset($cfg_sms["expires"])){
      $expires_seconds = intval($cfg_sms["expires"]);
    }else{
      $expires_seconds = 60;
    }

    $param = [];
    $param["username"] = $cfg_sms["username"];
    $param["password"] = $cfg_sms["password"];
    $param["productid"] = $cfg_sms["product_id"];
    $param["mobile"] = $log["mobile"];
    $param["content"] = $log["content"];
    $response_content = $this->http_post($cfg_sms["url"], $param);
    // $response_content = "1,201701121819232675";
    $data = ["id" => $id];
    if($response_content){
      $arr = explode(",", $response_content);
      $data["sms_no"] = $arr[1];
      $current_time = time();
      $data["expire_time"] = $current_time + $expires_seconds;
      if(intval($arr[0]) == 1){
        $data["send_status"] = "successed";
        $res = ["status" => true, "msg" => "短信发送成功"];
      }else{
        $data["send_status"] = "failed";
        $res["msg"] = "短信发送失败，请稍后再试";
      }
    }else{
      $data["send_status"] = "failed";
      $res["msg"] = "系统错误，请稍后再试";
    }

    $this->update($data);
    
    return $res;
  }

  /**
   * 生成短信验证码
   * @Author zhanghong
   * @Date   2017-01-13
   * @return string
   */
  private function generateCode(){
    $code = rand(100000, 999999);
    return $code;
  }
}