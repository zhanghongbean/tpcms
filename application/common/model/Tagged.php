<?php
// +---------------+-------------+------+-----+---------+----------------+
// | Field         | Type        | Null | Key | Default | Extra          |
// +---------------+-------------+------+-----+---------+----------------+
// | id            | int(11)     | NO   | PRI | NULL    | auto_increment |
// | tag_id        | int(11)     | YES  |     | 0       |                |
// | taggable_type | varchar(50) | YES  |     |         |                |
// | taggable_id   | int(11)     | YES  |     | 0       |                |
// | create_time   | int(11)     | YES  |     | 0       |                |
// | update_time   | int(11)     | YES  |     | 0       |                |
// +---------------+-------------+------+-----+---------+----------------+
namespace app\common\model;
class Tagged extends Base{
  
}