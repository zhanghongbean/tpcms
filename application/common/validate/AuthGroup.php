<?php
// +-------------+--------------+------+-----+---------+----------------+
// | Field       | Type         | Null | Key | Default | Extra          |
// +-------------+--------------+------+-----+---------+----------------+
// | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
// | name        | varchar(30)  | NO   |     |         |                |
// | title       | varchar(30)  | NO   |     |         |                |
// | rules       | varchar(200) | NO   |     |         |                |
// | create_time | int(11)      | NO   |     | 0       |                |
// | update_time | int(11)      | NO   |     | 0       |                |
// | status      | tinyint(1)   | YES  |     | 1       |                |
// | group_type  | varchar(30)  | YES  |     |         |                |
// | editor_id   | int(11)      | YES  |     | 0       |                |
// | sort        | int(11)      | YES  |     | 0       |                |
// | min_score   | int(11)      | YES  |     | 0       |                |
// | max_score   | int(11)      | YES  |     | 0       |                |
// +-------------+--------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
use think\Request;
class AuthGroup extends Validate{
  //Request::instance()->param('group_type')
  protected $rule = [
    "title" => "require|max:30",
    "rules" => "max:200",
    "sort" => "egt:0|elt:9999",
    "min_score" => "egt:0|elt:99999999",
    "max_score" => "egt:0|elt:99999999"
  ];

  protected $message = [
    "title.require" => "用户组名不能为空",
    "title.unique" => "用户组名已存在",
    "title.max" => "用户组名不能超过30个字符",
    "rules.max" => "权限长度不能超过200个字符",
    "sort.egt" => "排序编号必须大于等于0",
    "sort.elt" => "排序编号必须小于等于9999",
    "min_score.egt" => "最小积分必须大于等于0",
    "min_score.elt" => "最小积分必须小于等于99999999",
    "max_score.egt" => "最大积分必须大于等于0",
    "max_score.elt" => "最大积分必须小于等于99999999"
  ];

  protected $scene = [
    "manager" => ["title", "rules", "sort"],
    "member" => ["title", "min_score", "max_score", "sort"]
  ];
}