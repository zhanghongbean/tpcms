<?php
// +-------------+--------------+------+-----+---------+----------------+
// | Field       | Type         | Null | Key | Default | Extra          |
// +-------------+--------------+------+-----+---------+----------------+
// | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
// | name        | varchar(50)  | YES  |     |         |                |
// | title       | varchar(30)  | YES  |     |         |                |
// | type        | tinyint(1)   | YES  |     | 1       |                |
// | status      | tinyint(1)   | YES  |     | 1       |                |
// | condition   | varchar(100) | YES  |     |         |                |
// | create_time | int(11)      | NO   |     | 0       |                |
// | update_time | int(11)      | NO   |     | 0       |                |
// | parent_id   | int(11)      | YES  |     | 0       |                |
// | icon        | varchar(50)  | YES  |     |         |                |
// | islink      | tinyint(1)   | NO   |     | 1       |                |
// | sort        | int(11)      | YES  |     | 0       |                |
// | tips        | text         | YES  |     | NULL    |                |
// | editor_id   | int(11)      | YES  |     | 0       |                |
// | rule_type   | varchar(20)  | YES  |     |         |                |
// +-------------+--------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class AuthRule extends Validate{
  protected $rule = [
    "title" => "require|max:30",
    "name" => "max:30",
    "sort" => "require|between:0,9999",
    "icon" => "max:50",
  ];

  protected $message = [
    "title.require" => "菜单名称不能为空",
    "title.max" => "菜单名称不能超过30个字符",
    // "name.require" => "页面链接不能为空",
    "name.max" => "页面链接不能超过30个字符",
    "icon.max" => "ICON图标不能超过30个字符",
    "sort.between" => "排序编号必须在0-9999之间",
  ];
}