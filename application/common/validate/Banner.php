<?php
// +--------------+--------------+------+-----+---------+----------------+
// | Field        | Type         | Null | Key | Default | Extra          |
// +--------------+--------------+------+-----+---------+----------------+
// | id           | int(11)      | NO   | PRI | NULL    | auto_increment |
// | title        | varchar(60)  | YES  |     |         |                |
// | type         | varchar(15)  | YES  |     |         |                |
// | pc_cover_id  | int(11)      | YES  |     | 0       |                |
// | wap_cover_id | int(11)      | YES  |     | 0       |                |
// | status       | tinyint(1)   | YES  |     | 0       |                |
// | user_id      | int(11)      | YES  |     | 0       |                |
// | create_time  | int(11)      | YES  |     | 0       |                |
// | update_time  | int(11)      | YES  |     | 0       |                |
// | link_url     | varchar(255) | YES  |     |         |                |
// | start_time   | int(11)      | YES  |     | 0       |                |
// | end_time     | int(11)      | YES  |     | 0       |                |
// +--------------+--------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class Banner extends Validate{
  protected $rule = [
    "title" => "min:2|max:60",
    "type" => "require",
    "wap_cover_id" => "require",
  ];

  protected $field = [
    "title"  => "标题",
    "type"  => "分类",
    "wap_cover_id"  => "封面图片",
  ];

  protected $message = [
    "title.min" => "标题长度不能小于2个字符",
    "title.max" => "标题长度不能超过50个字符",
    "type" => "Banner类型不能为空",
    "wap_cover_id" => "封面图片不能为空",
  ];
}