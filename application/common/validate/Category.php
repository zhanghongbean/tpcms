<?php
// +-----------------+--------------+------+-----+---------+----------------+
// | Field           | Type         | Null | Key | Default | Extra          |
// +-----------------+--------------+------+-----+---------+----------------+
// | id              | int(11)      | NO   | PRI | NULL    | auto_increment |
// | name            | varchar(50)  | YES  |     |         |                |
// | title           | varchar(50)  | YES  |     |         |                |
// | meta_type       | varchar(50)  | YES  |     |         |                |
// | parent_id       | int(11)      | YES  |     | 0       |                |
// | sort_num        | int(11)      | YES  |     | 0       |                |
// | seo_title       | varchar(255) | YES  |     |         |                |
// | seo_keywords    | varchar(255) | YES  |     |         |                |
// | seo_description | varchar(255) | YES  |     |         |                |
// | template_index  | varchar(100) | YES  |     |         |                |
// | template_list   | varchar(100) | YES  |     |         |                |
// | template_show   | varchar(100) | YES  |     |         |                |
// | is_leaf         | tinyint(1)   | YES  |     | 1       |                |
// | status          | tinyint(1)   | YES  |     | 1       |                |
// | editor_id       | int(11)      | YES  |     | 0       |                |
// | create_time     | int(11)      | YES  |     | 0       |                |
// | update_time     | int(11)      | YES  |     | 0       |                |
// +-----------------+--------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class Category extends Validate{
  protected $rule = [
    "name" => "unique:category|min:2|max:50|alphaNum",
    "title" => "min:2|max:50",
    "meta_type" => "require|min:2|max:50",
    "parent_id" => "egt:0",
    "sort_num" => "egt:0",
    "template_index" => "max:100",
    "template_list" => "max:100",
    "template_show" => "max:100",
  ];

  protected $field = [
    "name"  => "分类名",
    "title"  => "分类标题",
    "meta_type"  => "分类类型",
    "parent_id"  => "Parent ID",
    "sort_num"  => "排序编号",
    "seo_title"  => "SEO标题",
    "seo_keywords"  => "SEO关键词",
    "seo_description"  => "SEO描述",
    "template_index"  => "首页模板",
    "template_list"  => "列表页模板",
    "template_show"  => "详细页模板",
    "is_leaf"  => "是否是终极分类",
    "status"  => "启用状态",
  ];

  protected $message = [
    "name.unique" => "分类名已存在",
    "name.min" => "分类名长度不能小于2个字符",
    "name.max" => "分类名长度不能超过50个字符",
    "name.alphaNum" => "分类名只能由字母和数字组成",
    "title.min" => "分类标题长度不能小于2个字符",
    "title.max" => "分类标题长度不能超过50个字符",
    "meta_type.require" => "分类类型不能为空",
    "meta_type.min" => "分类类型长度不能小于2个字符",
    "meta_type.max" => "分类类型长度不能超过50个字符",
    "parent_id.egt" => "Parent ID必须大于等于0",
    "sort_num.egt" => "排序编号必须大于等于0",
    "template_index" => "首页模板名长度不能超过100个字符",
    "template_list" => "列表页模板名长度不能超过100个字符",
    "template_show" => "详细页模板名长度不能超过100个字符",
  ];
}