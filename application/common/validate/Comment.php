<?php
// +--------------+--------------+------+-----+---------+----------------+
// | Field        | Type         | Null | Key | Default | Extra          |
// +--------------+--------------+------+-----+---------+----------------+
// | id           | int(11)      | NO   | PRI | NULL    | auto_increment |
// | user_id      | int(11)      | YES  |     | 0       |                |
// | belongs_type | varchar(20)  | YES  |     |         |                |
// | belongs_id   | int(11)      | YES  |     | 0       |                |
// | title        | varchar(100) | YES  |     |         |                |
// | content      | text         | YES  |     | NULL    |                |
// | ip           | int(11)      | NO   |     | 0       |                |
// | status       | tinyint(1)   | NO   |     | 0       |                |
// | editor_id    | int(11)      | YES  |     | 0       |                |
// | create_time  | int(11)      | YES  |     | 0       |                |
// | update_time  | int(11)      | YES  |     | 0       |                |
// +--------------+--------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class Comment extends Validate{
  protected $rule = [
    "user_id" => "egt:1",
    "title" => "min:5|max:60",
    "belongs_type" => "max:20",
    "belongs_id" => "require",
    "content" => "min:5",
  ];

  protected $field = [
    "user_id"  => "评论用户",
    "title"  => "标题",
    "belongs_type"  => "所属对象类型",
    "belongs_id"  => "所属对象ID",
  ];

  protected $message = [
    "user_id.egt" => "请先登录系统再评论",
    "title.min" => "标题长度不能小于5个字符",
    "title.max" => "标题长度不能超过60个字符",
    "belongs_type" => "所属对象类型不能为空",
    "belongs_id" => "所属对象ID不能为空",
    "content.min" => "评论内容不能少于5个字符",
  ];
}