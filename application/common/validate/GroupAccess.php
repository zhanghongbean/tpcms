<?php
// +-------------+-------------+------+-----+---------+----------------+
// | Field       | Type        | Null | Key | Default | Extra          |
// +-------------+-------------+------+-----+---------+----------------+
// | id          | int(11)     | NO   | PRI | NULL    | auto_increment |
// | uid         | int(11)     | NO   | MUL | 0       |                |
// | group_id    | int(11)     | NO   | MUL | 0       |                |
// | create_time | int(11)     | NO   |     | 0       |                |
// | update_time | int(11)     | NO   |     | 0       |                |
// | editor_id   | int(11)     | YES  |     | 0       |                |
// | user_type   | varchar(10) | YES  |     |         |                |
// +-------------+-------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class GroupAccess extends Validate{
  protected $rule = [
    "uid" => "require|gt:0",
    "group_id" => "require|gt:0",
    "user_type" => "require",
  ];

  protected $message = [
    "uid.require" => "用户ID不能为空",
    "uid.gt" => "用户ID不能为空",
    "group_id.require" => "用户组ID不能为空",
    "group_id.gt" => "用户组ID不能为空",
    "user_type" => "用户类型不能为空",
  ];
}