<?php
// +-----------------+--------------+------+-----+---------+----------------+
// | Field           | Type         | Null | Key | Default | Extra          |
// +-----------------+--------------+------+-----+---------+----------------+
// | id              | int(11)      | NO   | PRI | NULL    | auto_increment |
// | category_id     | int(11)      | YES  |     | 0       |                |
// | name            | varchar(50)  | YES  |     |         |                |
// | title           | varchar(255) | YES  |     |         |                |
// | sort_num        | int(11)      | YES  |     | 0       |                |
// | seo_title       | varchar(255) | YES  |     |         |                |
// | seo_keywords    | varchar(255) | YES  |     |         |                |
// | seo_description | varchar(255) | YES  |     |         |                |
// | description     | varchar(255) | YES  |     |         |                |
// | content         | text         | YES  |     | NULL    |                |
// | pc_cover_id     | int(11)      | YES  |     | 0       |                |
// | wap_cover_id    | int(11)      | YES  |     | 0       |                |
// | status          | tinyint(1)   | YES  |     | 0       |                |
// | user_id         | int(11)      | YES  |     | 0       |                |
// | create_time     | int(11)      | YES  |     | 0       |                |
// | update_time     | int(11)      | YES  |     | 0       |                |
// | is_origin       | tinyint(1)   | YES  |     | 0       |                |
// | origin_author   | varchar(100) | YES  |     |         |                |
// | star_up         | int(11)      | YES  |     | 0       |                |
// | star_down       | int(11)      | YES  |     | 0       |                |
// +-----------------+--------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class News extends Validate{
  protected $rule = [
    "title" => "min:2|max:100",
    "category_id" => "require",
    "wap_cover_id" => "require",
    // "video_url" => "url",
    // "buy_url" => "url",
    // "content" => "require",
  ];

  protected $field = [
    "title"  => "标题",
    "category_id"  => "分类",
    "wap_cover_id"  => "封面图片",
    "video_url"  => "视频URL",
    "buy_url"  => "购买URL",
    "content"  => "正文",
  ];

  protected $message = [
    "title.min" => "标题长度不能小于2个字符",
    "title.max" => "标题长度不能超过50个字符",
    "category_id" => "文章分类不能为空",
    "wap_cover_id" => "封面图片不能为空",
    "video_url" => "视频URL不正确",
    "buy_url" => "购买URL不能为空",
    // "content" => "正文不能为空",
  ];
}