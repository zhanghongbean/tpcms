<?php
// +--------------+-------------+------+-----+---------+----------------+
// | Field        | Type        | Null | Key | Default | Extra          |
// +--------------+-------------+------+-----+---------+----------------+
// | id           | int(11)     | NO   | PRI | NULL    | auto_increment |
// | user_id      | int(11)     | YES  |     | 0       |                |
// | related_type | varchar(50) | YES  |     |         |                |
// | related_id   | int(11)     | YES  |     | 0       |                |
// | client_ip    | bigint(20)  | NO   |     | NULL    |                |
// | create_time  | int(11)     | YES  |     | 0       |                |
// | update_time  | int(11)     | YES  |     | 0       |                |
// | star_type    | varchar(6)  | YES  |     |         |                |
// +--------------+-------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class StarLog extends Validate{
  protected $rule = [
    "user_id" => "egt:0",
    "related_type" => "require",
    "related_id" => "gt:0",
    "star_type" => "require|in:up,down",
    // "client_ip" => "require",
  ];

  protected $message = [
    "user_id.egt" => "用户ID不能为空",
    "related_type.require" => "关联类型不能为空",
    "related_id.gt" => "关联对象ID不能为空",
    "star_type.require" => "点赞类型不能为空",
  ];
}