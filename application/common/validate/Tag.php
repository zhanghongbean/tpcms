<?php
// +-------------+-------------+------+-----+---------+----------------+
// | Field       | Type        | Null | Key | Default | Extra          |
// +-------------+-------------+------+-----+---------+----------------+
// | id          | int(11)     | NO   | PRI | NULL    | auto_increment |
// | name        | varchar(50) | YES  |     |         |                |
// | sort_num    | int(11)     | YES  |     | 0       |                |
// | is_hot      | tinyint(1)  | YES  |     | 0       |                |
// | status      | tinyint(1)  | YES  |     | 1       |                |
// | editor_id   | int(11)     | YES  |     | 0       |                |
// | create_time | int(11)     | YES  |     | 0       |                |
// | update_time | int(11)     | YES  |     | 0       |                |
// +-------------+-------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class Tag extends Validate{
  protected $rule = [
    "name" => "min:2|max:50",
  ];

  protected $field = [
    "name"  => "标签名",
  ];

  protected $message = [
    "name.min" => "标签名长度不能小于2个字符",
    "name.max" => "标签名长度不能超过50个字符",
  ];
}