<?php
// +---------------+-------------+------+-----+---------+----------------+
// | Field         | Type        | Null | Key | Default | Extra          |
// +---------------+-------------+------+-----+---------+----------------+
// | id            | int(11)     | NO   | PRI | NULL    | auto_increment |
// | tag_id        | int(11)     | YES  |     | 0       |                |
// | taggable_type | varchar(50) | YES  |     |         |                |
// | taggable_id   | int(11)     | YES  |     | 0       |                |
// | create_time   | int(11)     | YES  |     | 0       |                |
// | update_time   | int(11)     | YES  |     | 0       |                |
// +---------------+-------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class Tagged extends Validate{
  protected $rule = [
    "taggable_type" => "max:50|require",
    "taggable_id" => "gt:0|require",
    "tag_id" => "gt:0|require",
  ];

  protected $field = [
    "taggable_type"  => "关联类型",
    "taggable_id"  => "关联ID",
    "tag_id"  => "标签ID",
  ];

  protected $message = [
    "taggable_type.require" => "关联类型不能为空",
    "taggable_type.max" => "关联类型长度不能超过50个字符",
    "taggable_id" => "关联ID不能为空",
    "tag_id" => "标签ID不能为空",
  ];
}