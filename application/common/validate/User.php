<?php
// +------------------+--------------+------+-----+---------+----------------+
// | Field            | Type         | Null | Key | Default | Extra          |
// +------------------+--------------+------+-----+---------+----------------+
// | id               | int(11)      | NO   | PRI | NULL    | auto_increment |
// | name             | varchar(60)  | NO   |     |         |                |
// | password         | varchar(64)  | NO   |     |         |                |
// | nickname         | varchar(50)  | YES  |     |         |                |
// | email            | varchar(50)  | YES  |     |         |                |
// | url              | varchar(100) | YES  |     |         |                |
// | avatar           | varchar(200) | YES  |     |         |                |
// | gender           | varchar(10)  | YES  |     |         |                |
// | birthday         | date         | YES  |     | NULL    |                |
// | signature        | varchar(200) | YES  |     |         |                |
// | last_login_ip    | bigint(20)   | YES  |     | 0       |                |
// | last_login_time  | int(11)      | YES  |     | 0       |                |
// | activation_key   | varchar(60)  | YES  |     |         |                |
// | status           | tinyint(1)   | YES  |     | 1       |                |
// | active_score     | int(11)      | YES  |     | 0       |                |
// | user_type        | varchar(20)  | YES  |     |         |                |
// | active_coin      | int(11)      | YES  |     | 0       |                |
// | mobile           | varchar(20)  | YES  |     |         |                |
// | create_time      | int(11)      | YES  |     | 0       |                |
// | update_time      | int(11)      | YES  |     | 0       |                |
// | editor_id        | int(11)      | YES  |     | 0       |                |
// | manager_group_id | int(11)      | YES  |     | 0       |                |
// | is_locked        | tinyint(1)   | YES  |     | 0       |                |
// | total_score      | int(11)      | YES  |     | 0       |                |
// | total_coin       | int(11)      | YES  |     | 0       |                |
// | member_group_id  | int(11)      | YES  |     | 0       |                |
// | province         | int(11)      | YES  |     | 0       |                |
// | city             | int(11)      | YES  |     | 0       |                |
// | address          | varchar(255) | YES  |     |         |                |
// | description      | varchar(255) | YES  |     |         |                |
// +------------------+--------------+------+-----+---------+----------------+
namespace app\common\validate;
use think\Validate;
class User extends Validate{
  protected $rule = [
    "name" => "min:2|max:30",
    "email" => "email|max:50|unique:user",
    "nickname" => "min:2|max:30",
    "mobile" => "max:20|unique:user",
    "password" => "confirm:confirm_password|length:6,20",
    "manager_group_id" => "gt:0",
    "url" => "url",
    "user_type" => "min:1",
    "active_score" => "egt:0",
    "total_score" => "egt:0",
    "active_coin" => "egt:0",
    "total_coin" => "egt:0",
  ];

  protected $field = [
    // "name"  => "登录名",
    "email"  => "邮箱",
    "nickname"  => "用户昵称",
    "mobile"  => "手机号码",
    "password"  => "登录密码",
    "manager_group_id"  => "管理员组",
    "url" => "url",
    "user_type" => "用户类型",
    "active_score" => "可用积分",
    "total_score" => "总积分",
    "active_coin" => "可用元宝",
    "total_coin" => "总元宝",
  ];

  protected $message = [
    "name.min" => "登录名长度不能小于2个字符",
    "name.max" => "登录名长度不能超过30个字符",
    "email.email" => "注册邮箱格式不正确",
    "email.max" => "注册邮箱长度不能超过50个字符",
    "nickname.min" => "用户昵称长度不能小于2个字符",
    "nickname.max" => "用户昵称长度不能超过30个字符",
    "mobile.max" => "手机号码格式不正确",
    "password.confirm" => "两次输入的密码不一致",
    "password.length" => "登录密码长度必须在6-20之间",
    "manager_group_id.gt" => "管理员组不能为空",
    "url" => "url格式不正确",
    "user_type" => "用户类型不能为空",
    "active_score.egt" => "可用积分必须大于等于0",
    "total_score.egt" => "总积分必须大于等于0",
    "active_coin.egt" => "可用元宝必须大于等于0",
    "total_coin.egt" => "总元宝必须大于等于0",
  ];

  protected $scene = [
    "manager" => ["email", "mobile", "password", "manager_group_id", "user_type", "nickname"],
    "member" => ["email", "mobile", "password", "user_type", "nickname"],
    "score_coin" => ["active_score", "total_score", "active_coin", "total_coin"],
  ];
}