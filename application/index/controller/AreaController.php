<?php
namespace app\index\controller;
use app\index\controller\BaseController;
use app\common\model\Area;
class AreaController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new Area();
  }

  public function index(){
    $name = $this->request->param("name");
    $parent_id = $this->request->param("parent_id");
    $select_id = $this->request->param("select_id");
    $allow_blank = $this->request->param("allow_blank");

    $areas = $this->Model->children(intval($parent_id));
    switch($name) {
    case "province":
      $blank_name = "请选择所在省";
      break;
    case "city":
      $blank_name = "请选择所在城市";
      break;
    default:
      $blank_name = "请选择";
      break;
    }
    $this->assign("blank_name", $blank_name);
    $this->assign("select_id", intval($select_id));
    $this->assign("allow_blank", $allow_blank);
    $this->assign("areas", $areas);
    return $this->fetch();
  }
}