<?php
namespace app\index\controller;
use think\Controller;
use think\Request;
use think\Config;
use think\Session;
class AttachController extends Controller{
  public function _initialize($skip_action = []){
    parent::_initialize();

    // 读取网站设置
    settings();
    $site_root = Config::get("site_root");
    $this->assign("site_root", $site_root);
    
    $this->request = Request::instance();
    $current_action = $this->request->action();
    $ctrl_name = $this->request->controller();
    $ctrl_name = strtolower($ctrl_name);
    if(!in_array($ctrl_name, array("star_log"))){
      Session::flash("back_url", $this->request->url());
    }

    $login_user = login_user();
    if(empty($login_user) && !in_array($current_action, $skip_action)){
      if($this->request->isGet()){
        $this->redirect("login/login");
      }else{
        $this->success("请登录系统", "login/login", '', 0);
      }
    }
  }
}