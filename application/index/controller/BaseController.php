<?php
namespace app\index\controller;
use think\Controller;
use think\Request;
use think\Config;
use app\common\model\Category;
use app\common\model\Options;
use app\weixin\model\WeixinApp;
class BaseController extends Controller{
  public function _initialize(){
    parent::_initialize();

    // 部署环境
    $environment = Config::get("environment");
    $this->assign("environment", $environment);

    // 读取配置信息
    $OptionsModel = new Options();
    $options = $OptionsModel->readAndSetConfig();

    $site_root = Config::get("site_root");
    $this->assign("site_root", $site_root);

    $site_options = Config::get("site_options");
    $this->assign("site_options", $site_options);

    // 当前页面记录
    $this->request = Request::instance();
    //当前登录用户
    $this->current_user = login_member();
    $this->assign("current_user", $this->current_user);

    // Article分类信息（因为是左侧菜单，所以每个方法都需要查询）
    $CategoryModel = new Category();
    $this->note_categories = $CategoryModel->mateCacheList("Note");
    $this->assign("note_categories", $this->note_categories);

    $this->news_categories = $CategoryModel->mateCacheList("News");
    $this->assign("news_categories", $this->news_categories);

    $weixin_app = session_current_weixin_app();
    if(empty($weixin_app)){
      $config_apps = Config::get("weixin_apps");
      if(!empty($config_apps) && isset($config_apps["home"])){
        $cfg_app = $config_apps["home"];
        if(isset($cfg_app["appid"])){
          $weixin_app = change_current_weixin_app($cfg_app["appid"]);
        }
      }
    }
    $this->weixin_app = $weixin_app;
    $this->assign("weixin_app", $this->weixin_app);

    $this->weixin_share_info = [];

    // $weixin_user = session_current_weixin_user();
    // if(empty($weixin_user)){
    //   $config_apps = Config::get("weixin_apps");
    //   if(!empty($config_apps) && isset($config_apps["home"])){
    //     $cfg_app = $config_apps["home"];
    //     if(isset($cfg_app["openid"])){
    //       $weixin_user = change_current_weixin_user($cfg_app["openid"]);
    //     }
    //   }
    // }
    // $this->weixin_user = $weixin_user;
    // $this->assign("weixin_user", $this->weixin_user);

    $this->init_left_menu_list();
  }

  /**
   * 页面左侧active菜单项
   * @Author zhanghong
   * @Date   2017-04-08
   * @return array
   */
  function init_left_menu_list(){
    $list = [];
    $ctrl_name = $this->request->controller();
    $ctrl_name = strtolower($ctrl_name);

    $act_name = $this->request->action();
    $act_name = strtolower($act_name);

    switch ($ctrl_name) {
    case "index":
      switch ($act_name) {
      case "index":
        array_push($list, "site_home");
        break;
      }
      break;
    case "news":
      array_push($list, "news_index");
      // switch ($act_name) {
      // case "index":
      // case "read":
      //   array_push($list, "news_index");
      //   break;
      // }
      break;
    case "note":
      array_push($list, "nav_note");
      switch ($act_name) {
      case "index":
        array_push($list, "note_{$act_name}");
        break;
      case "category":
        $name = $this->request->param("name");
        array_push($list, "note_category_{$name}");
        break;
      case "personal":
        $list = ["nav_user", "note_personal"];
        break;
      }
      break;
    case "member":
      array_push($list, "nav_user");
      switch ($act_name) {
      case "index":
      case "update":
        array_push($list, "member_{$act_name}");
        break;
      default:
        break;
      }
      break;
    }

    $this->assign("current_controller", $ctrl_name);
    $this->assign("current_action", $act_name);
    $this->assign("active_menus", $list);
  }

  public function _empty($name){
    //把所有城市的操作解析到city方法
    if($this->request->isAjax()){
      $this->error("您访问的页面不存在");
    }else{
      $this->redirect("/");
    }
  }

  /**
   * 页面SEO信息
   * @Author zhanghong
   * @Date   2017-05-16
   * @param  string     $title       [description]
   * @param  string     $description [description]
   * @param  string     $keywords    [description]
   * @return [type]                  [description]
   */
  protected function action_seo($title = '', $description = '', $keywords = ''){
    $site_seo = \think\Config::get("site_options");
    $site_name = $site_seo["site_name"];

    if(empty($title)){
      $title = $site_name;
    }else{
      $title = $title."－".$site_name;
    }

    if(empty($description)){
      $description = $site_seo["site_seo_description"];
    }

    if(empty($keywords)){
      $keywords = $site_seo["site_seo_keywords"];
    }

    $this->seo = ["title" => $title, "description" => $description, "keywords" => $keywords];
    $this->assign("seo", $this->seo);
  }

  protected function fetch($template = '', $vars = [], $replace = [], $config = []){
    if(!$this->request->isAjax()){
      // ajax请求不需要微信页面签名信息
      $this->weixin_js_sign();
    }
    return parent::fetch($template, $vars, $replace, $config);
  }

  /**
   * 微信签名方法
   * @Author zhanghong
   * @Date   2017-05-08
   * @return 微信签名结果
   */
  public function weixin_js_sign(){
    if(!empty($this->weixin_app)){
      $appid = $this->weixin_app["appid"];
      // 获取当前URL地址,含域名但不含QUERY_STRING
      $current_url = $this->request->baseUrl(true);
      $AppModel = new WeixinApp();
      $sign_package = $AppModel->getJsSign($appid, $current_url);
    }else{
      $sign_package = [
        "appid" => "",
        "noncestr" => "",
        "timestamp" => time(),
        "url" => "",
        "signature" => "",
      ];
    }
    // 获取当前域名
    $domain = $this->request->domain();

    // 默认分享链接是当前页面
    $sign_package["share_url"] = $this->request->url(true);
    // 默认分享图片 -- 不带域名的path
    $sign_package["image"] = "/static/index/images/weixin_share.jpg";

    if(isset($this->seo)){
      $sign_package["title"] = $this->seo["title"];
      $sign_package["description"] = $this->seo["description"];
    }else{
      $site_seo = Config::get("site_options");
      $sign_package["title"] = $site_seo["site_name"];
      $sign_package["description"] = $site_seo["site_seo_description"];
    }

    if(!empty($this->weixin_share_info) && is_array($this->weixin_share_info)){
      $sign_package = array_merge($sign_package, $this->weixin_share_info);
    }

    $sign_package["image"] = $domain.$sign_package["image"];

    // 测试使用
    $this->sign_package = $sign_package;

    $this->assign("sign_package", $sign_package);
  }
}