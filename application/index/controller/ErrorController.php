<?php
namespace app\index\controller;
use think\Controller;
use think\Request;
class ErrorController extends Controller{
  public function _empty($name){
    //把所有城市的操作解析到city方法
    if($this->request->isAjax()){
      $this->error("您访问的页面不存在");
    }else{
      $this->redirect("/");
    }
  }
}