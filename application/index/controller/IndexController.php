<?php
namespace app\index\controller;
use app\index\controller\BaseController;
use app\common\model\User;
use app\common\model\Article;
use app\common\model\Banner;
class IndexController extends BaseController{
  public function _initialize($skip_action = []){
    $skip_action = ["index"];
    parent::_initialize($skip_action);
  }

  public function index(){
    $UserModel = new User();
    $top_users = $UserModel->lastLoginSelect();
    $this->assign("top_users", $top_users);

    $ArticleModel = new Article();
    $top_notes = $ArticleModel->topTypeList("Note");
    $this->assign("top_notes", $top_notes);

    $BannerModel = new Banner();
    $home_banners = $BannerModel->activeTopSelect("Home");
 
    $this->assign("home_banners", $home_banners);

    $breadcrumb = [
      ["title" => "首页", "icon" => "fa-home"]
    ];
    $this->assign("breadcrumb", $breadcrumb);
    $this->action_seo("");
    return $this->fetch();
  }
}