<?php
namespace app\index\controller;
use app\index\controller\BaseController;
use think\Session;
use app\common\model\User;
use app\common\model\Banner;
class LoginController extends BaseController{
  function index(){
    $this->redirect("login");
  }

  function login(){
    $manager = login_member();
    if($manager){
      if($this->request->isGet()){
        $this->redirect("index/index");
      }else{
        $this->success("登录成功", "index/index", '', 0);
      }
    }
    
    if($this->request->isPost()){
      $name = $this->request->param("name");
      $password = $this->request->param("password");
      $Model = new User();
      $user = $Model->memberLogin($name, $password);
      if(empty($user)){
        $this->error("登录失败");
      }else{
        $is_remember = $this->request->param("is_remember");
        if(!empty($is_remember)){
          $expire = 7*24*3600;
        }else{
          $expire = 24*3600;
        }
        Session::init(["expire" => $expire]);
        Session::set("member", $user);
        $back_url = Session::pull('back_url');
        if(empty($back_url)){
          $back_url = url("index/index");
        }
        $this->success("登录成功", $back_url);
      }
    }else{
      $BannerModel = new Banner();
      $banner = $BannerModel->randFindByType("login");
      $this->assign("banner", $banner);
      
      $this->action_seo("登录");
      return $this->fetch();
    }
  }

  function logout(){
    Session::delete("member");
    $this->redirect("index/index");
  }
}