<?php
namespace app\index\controller;
use app\index\controller\UserController;
use app\common\model\User;
use think\Session;
class MemberController extends UserController{
  public function _initialize($skip_action = []){
    parent::_initialize($skip_action);
    $this->Model = new User();
    $this->user_type = "member";
  }

  public function index(){
    $breadcrumb = [
      ["title" => "个人首页", "icon" => "fa-user"]
    ];
    $this->assign("breadcrumb", $breadcrumb);
    $this->action_seo("个人首页");
    return $this->fetch();
  }

  public function update(){
    $user_id = login_member_id();
    if($this->request->isPost()){
      $data = $this->getPostData();
      $data["id"] = $user_id;
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $user = $this->Model->reloadUserInfo($user_id);
        Session::set("member", $user);
        Session::flash("notice", "更新个人信息成功");
        $this->success($res["msg"]);
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $user = $this->Model->where(array("id" => $user_id))->find();
      $this->assign("user", $user);

      $notice = Session::pull('notice');
      $this->assign("notice", $notice);

      $breadcrumb = [
        ["title" => "个人信息", "icon" => "fa-user"]
      ];
      $this->assign("breadcrumb", $breadcrumb);
      $this->action_seo("个人信息");

      return $this->fetch();
    }
  }

  public function change_password(){
    $user_id = login_member_id();
    if($this->request->isPost()){
      $data = [];
      $param_name = ["old_password", "password", "confirm_password"];
      $post = $this->request->post();
      foreach ($param_name as $key) {
        if(isset($post[$key])){
          $val = $post[$key];
          $val = trim($val);
        }else{
          $val = "";
        }

        $data[$key] = $val;
      }

      $res = $this->Model->changePassword($user_id, $data);
      if($res["status"]){
        Session::flash("notice", "更新密码成功");
        $this->success($res["msg"]);
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $notice = Session::pull('notice');
      $this->assign("notice", $notice);

      $breadcrumb = [
        ["title" => "修改密码", "icon" => "fa-lock"]
      ];
      $this->assign("breadcrumb", $breadcrumb);
      $this->action_seo("修改密码");
      return $this->fetch();
    }
  }

  public function ajax_valid_password(){
    $password = $this->request->post("old_password");
    $user_id = login_member_id();
    $valid_res = $this->Model->validPassword($user_id, $password);
    echo($valid_res);
  }

  public function change_avatar(){
    $user_id = login_member_id();
    if($this->request->isPost()){
      $data = ["user_type" => $this->user_type];
      $data["avatar"] = $this->request->post("avatar");
      $data["id"] = $user_id;
      $res = $this->Model->updateBaseInfo($data);
      if($res["status"]){
        $user = $this->Model->reloadUserInfo($user_id);
        Session::set("member", $user);
        Session::flash("notice", "更新个人头像成功");
        $this->success($res["msg"]);
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $user = $this->Model->where(array("id" => $user_id))->find();
      $this->assign("user", $user);

      $notice = Session::pull('notice');
      $this->assign("notice", $notice);

      $breadcrumb = [
        ["title" => "头像修改", "icon" => "fa-camera"]
      ];
      $this->assign("breadcrumb", $breadcrumb);
      $this->action_seo("头像修改");

      return $this->fetch();
    }
  }

  public function photo(){
    return $this->fetch();
  }

  public function _empty($name){
    //把所有城市的操作解析到city方法
    if($this->request->isAjax()){
      $this->error("您访问的页面不存在");
    }else{
      $this->redirect("index");
    }
  }

  private function getPostData(){
    $atl_name = $this->request->action();
    $data = ["user_type" => $this->user_type];
    $param_name = ["name", "nickname", "description", "email", "gender", "birthday", "province", "city", "address"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      switch ($key) {
      case "birthday":
        if(empty($val)){
          $val = NULL;
        }
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }
}