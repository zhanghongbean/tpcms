<?php

namespace app\index\controller;
use app\common\model\Article;
use app\common\model\StarLog;
use app\common\model\Category;
use app\common\model\Tag;
use app\common\model\Upload;
use app\common\model\Comment;
class NewsController extends UserController{
  public function _initialize($skip_action = []){
    $skip_action = ["index", "category", "read", "user"];
    parent::_initialize($skip_action);
    $this->Model = new Article();
    // 对象类型
    $this->type = "News";
    // 在关联表里对应的类型
    $this->related_type = "Article";
  }

  public function index(){
    $param = $this->request->param();
    $default_param = ["sort_type" => "time_desc"];
    $res = $this->Model->memberSelect(array_merge($default_param, $param, ["type" => $this->type]));
    $this->assign("res", $res);
    if($this->request->isAjax()){
      // 这里不能用return 来返回fetch结果，否则引号和“/”会被转义
      $html = $this->fetch("ajax_list");
      echo($html);
      exit();
    }

    $this->assign("param", $param);
    $page_title = "造物行走";
    $this->assign("page_title", $page_title);

    $breadcrumb = [
      ["title" => $page_title, "icon" => "fa-newspaper-o"]
    ];
    $this->assign("breadcrumb", $breadcrumb);
    $this->action_seo($page_title);

    return $this->fetch();
  }

  // public function category(){
  //   $category_name = $this->request->param("name");
  //   $CategoryModel = new Category();
  //   $category = $CategoryModel->where(["status" => true, "name" => $category_name, "meta_type" => $this->type])->find();
  //   if(empty($category)){
  //     $this->redirect("index");
  //   }

  //   $param = $this->request->param();
  //   $default_param = ["sort_type" => "star_desc"];
  //   $res = $this->Model->memberSelect(array_merge($default_param, $param, ["type" => $this->type, "category_id" => $category["id"]]));
  //   $this->assign("res", $res);
  //   if($this->request->isAjax()){
  //     // 这里不能用return 来返回fetch结果，否则引号和“/”会被转义
  //     $html = $this->fetch("ajax_list");
  //     echo($html);
  //     exit();
  //   }

  //   $this->assign("param", $param);
  //   $page_title = $category["title"];
  //   $this->assign("page_title", $page_title);

  //   $breadcrumb = [
  //     ["title" => $page_title, "icon" => "fa-newspaper-o"]
  //   ];
  //   $this->assign("breadcrumb", $breadcrumb);
  //   $this->action_seo($page_title);

  //   return $this->fetch("index");
  // }

  // public function personal(){
  //   $param = $this->request->param();
  //   if(!is_array($param)){
  //     $param = [];
  //   }
  //   // 显示编辑、删除按钮
  //   $this->assign("show_options", true);
  //   $default_param = ["sort_type" => "star_desc"];
  //   $res = $this->Model->memberSelect(array_merge($default_param, $param, ["type" => $this->type, "personal" => true]));
  //   $this->assign("res", $res);
  //   if($this->request->isAjax()){
  //     // 这里不能用return 来返回fetch结果，否则引号和“/”会被转义
  //     $html = $this->fetch("ajax_list");
  //     echo($html);
  //     exit();
  //   }
    
  //   $this->assign("param", $param);

  //   $page_title = "我的新闻";
  //   $breadcrumb = [
  //     ["title" => $page_title, "icon" => "fa-newspaper-o"]
  //   ];
  //   $this->assign("breadcrumb", $breadcrumb);
  //   $this->action_seo($page_title);
  //   return $this->fetch();
  // }

  // public function create(){
  //   if($this->request->isPost()){
  //     $data = $this->getPostData();
  //     $res = $this->Model->createOrUpdate($data);
  //     if($res["status"]){
  //       $this->success($res["msg"], 'personal');
  //     }else{
  //       $this->error($res["msg"]);
  //     }
  //   }else{
  //     $this->assign("article", ["status" => 0, "is_origin" => 1, "wap_cover_id" => 0, "pc_cover_id" => 0]);
  //     $this->assign("tag_names", "");

  //     $page_title = "添加新闻";
  //     $breadcrumb = [
  //       ["title" => "我的新闻", "url" => url('personal')],
  //       ["title" => $page_title, "icon" => "fa-newspaper-o"]
  //     ];
  //     $this->assign("breadcrumb", $breadcrumb);
  //     $this->action_seo($page_title);

  //     return $this->fetch("form");
  //   }
  // }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read(){
    $id = $this->request->param("id");
    $create_time = $this->request->param("ct");
    $option = ["create_time" => intval($create_time)];
    $article = $this->Model->relatedRead($this->type, $id, $option);
    if(empty($article)){
      $this->redirect("personal");
    }
    
    $TagModel = new Tag();
    $tags = $TagModel->searchByRelatedItem($this->related_type, $id);

    // 同一作者的其它作品
    $related_articles = $this->Model->sameAuthorList($article["id"], $article["user_id"]);

    $video_info = match_video_info($article["video_url"]);

    $this->assign("tags", $tags);
    $this->assign("article", $article);
    $this->assign("video_info", $video_info);
    $this->assign("related_articles", $related_articles);

    $StarLog = new StarLog();
    $log_res = $StarLog->relatedNewestSelect($this->related_type, $id);
    $this->assign("log_res", $log_res);

    // 手机版隐藏顶部的搜索栏
    $this->assign("hide_mobile_search", true);

    $this->weixin_share_info["title"] = $article["title"];
    if(!empty($article["seo_description"])){
      $this->weixin_share_info["description"] = $article["seo_description"];
    }else if(!empty($article["description"])){
      $this->weixin_share_info["description"] = $article["description"];
    }
    if(isset($article["wap_cover_path"]) && !empty($article["wap_cover_path"])){
      $this->weixin_share_info["image"] = $article["wap_cover_path"];
    }

    $CommentModel = new Comment();
    $comment_res = $CommentModel->belongsSelect("Article", $article['id'], 1);
    $this->assign("comment_res", $comment_res);

    $this->action_seo($article["category_title"]);

    return $this->fetch();
  }

  // /**
  //  * 显示编辑资源表单页.
  //  *
  //  * @param  int  $id
  //  * @return \think\Response
  //  */
  // public function edit(){
  //   $id = $this->request->param("id");
  //   $article = $this->Model->where("type", $this->type)
  //                          ->where("id", $id)
  //                          ->where("user_id", $this->current_user["id"])
  //                          ->find();
  //   if(empty($article)){
  //     $this->redirect('personal');
  //   }

  //   if($this->request->isPost()){
  //     $data = $this->getPostData();
  //     $res = $this->Model->createOrUpdate($data);
  //     if($res["status"]){
  //       $this->success($res["msg"], 'personal');
  //     }else{
  //       $this->error($res["msg"]);
  //     }
  //   }else{
  //     $this->assign("article", $article);

  //     $tagModel = new Tag();
  //     $tag_names = $tagModel->searchNameByRelatedItem($this->related_type, $id);
  //     $this->assign("tag_names", implode(",", $tag_names));

  //     $breadcrumb = [
  //       ["title" => "我的新闻", "url" => url('personal')],
  //       ["title" => "编辑新闻-".$article["title"], "icon" => "fa-newspaper-o"]
  //     ];
  //     $this->assign("breadcrumb", $breadcrumb);
  //     $this->action_seo("编辑新闻");
      
  //     return $this->fetch("form");
  //   }
  // }

  // /**
  //  * 删除指定资源
  //  *
  //  * @param  int  $id
  //  * @return \think\Response
  //  */
  // public function delete(){
  //   if($this->request->isDelete()){
  //     $id = $this->request->param("id");
  //     $map = array(
  //       "type" => $this->type,
  //       "id" => intval($id),
  //       "user_id" => $this->current_user["id"],
  //     );
  //     $deleted_count = Article::destroy($map);
  //     if($deleted_count){
  //       $msg = "删除成功";
  //     }else{
  //       $msg = "删除记录不存在";
  //     }
  //     $this->success($msg, url("personal"));  
  //   }else{
  //     $this->error("您请求的方法不存在", url("personal"));
  //   }
  // }

  public function _empty($name){
    //把所有城市的操作解析到city方法
    if($this->request->isAjax()){
      $this->error("您访问的页面不存在");
    }else{
      $this->redirect("index");
    }
  }

  private function getPostData(){
    $atl_name = $this->request->action();
    $data = ["type" => $this->type];
    $param_name = ["id", "category_id", "title", "seo_title", "seo_keywords", "seo_description", "description", "content", "status", "tag_names", "wap_cover_id", "is_origin"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      if($key == "id" && empty($val)){
        continue;
      }

      switch ($key) {
      case "sort_num":
      case "wap_cover_id":
        $val = intval($val);
        break;
      case "status":
      case "is_origin":
        if(empty($val)){
          $val = 0;
        }else{
          $val = 1;
        }
        break;
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }


    if($data["is_origin"]){
      $data["origin_author"] = "";
    }else{
      $origin_author = $this->request->post("origin_author");
      $data["origin_author"] = trim($origin_author);
    }
    return $data;
  }
}
