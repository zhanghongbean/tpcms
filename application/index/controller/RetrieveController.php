<?php
namespace app\index\controller;
use app\index\controller\BaseController;
use app\common\model\User;
use app\common\model\Banner;
use app\common\model\SmsLog;
class RetrieveController extends BaseController{
  public function _initialize(){
    parent::_initialize();
    $this->Model = new User();
    $this->user_type = "member";
  }

  public function index(){
    $this->redirect(url("create"));
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $code = $this->request->post("code");
      $LogModel = new SmsLog();
      $code_res = $LogModel->validSendCode("retrieve", $data["mobile"], $code);
      if($code_res["status"] == false){
        $this->error($code_res["msg"]);
      }
      $res = $this->Model->retrievePassword($data);
      if($res["status"]){
        $this->success($res["msg"], 'login/login');
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $BannerModel = new Banner();
      $banner = $BannerModel->randFindByType("login");
      $this->assign("banner", $banner);

      $this->action_seo("找回密码");

      return $this->fetch();
    }
  }

  public function ajax_check_present(){
    if($this->Model->isFieldValueUnique()){
      echo("false");
    }else{
      echo("true");
    }
  }

  public function sms_code(){
    if($this->request->isPost()){
      $LogModel = new SmsLog();
      $mobile = $this->request->param("mobile");
      $res = $LogModel->sendRetrieveMsg($mobile);
    }else{
      $res = ["status" => false, "msg" => "请求方式不正确"];
    }
    return json($res);
  }

  private function getPostData(){
    $atl_name = $this->request->action();
    $data = [];
    $param_name = ["mobile", "password", "confirm_password"];
    $param = $this->request->post();
    foreach ($param_name as $key) {
      if(isset($param[$key])){
        $val = $param[$key];
      }else{
        $val = "";
      }

      switch ($key) {
      default:
        $val = trim($val);  
      }
      $data[$key] = $val;
    }
    return $data;
  }
}