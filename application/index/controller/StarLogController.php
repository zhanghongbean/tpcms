<?php

namespace app\index\controller;
use app\common\model\StarLog;
class StarLogController extends UserController{
  public function _initialize($skip_action = []){
    $skip_action = [];
    parent::_initialize($skip_action);
    $this->Model = new StarLog();
  }

  public function create(){
    if($this->request->isPost()){
      $data = $this->getPostData();
      $res = $this->Model->createItem($data);
      if($res["status"]){
        $this->success($res["msg"]);
      }else{
        $this->error($res["msg"]);
      }
    }else{
      $this->error("请求方法不存在", url('index/index'));
    }
  }

  private function getPostData(){
    $atl_name = $this->request->action();
    $data = [];
    $param_name = ["related_id", "related_type", "star_type"];
    $param = $this->request->post();
    // $param = $this->request->param();
    foreach ($param_name as $key) {
      if(isset($param[$key])){
        $val = $param[$key];
        $val = trim($val);
      }else{
        $val = "";
      }

      switch ($key) {
      case "related_id":
        $val = intval($val);
        break;
      case "related_type":
        $val = strtolower($val);
        break;
      }
      $data[$key] = $val;
    }
    return $data;
  }
}
