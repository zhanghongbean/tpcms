<?php

namespace app\index\controller;
use app\common\model\Upload;
class UploadController extends AttachController{
  public function _initialize($skip_action = []){
    parent::_initialize($skip_action);
    $this->Model = new Upload();
  }

  public function create(){
    $backcall = $this->request->param("backcall");
    $backcall_name = $this->request->param("backcall_name");
    $width = $this->request->param("width");
    $height = $this->request->param("height");
    $image = $this->request->param("image");

    if($this->request->isPost()){
      $file = $this->request->file('image');
      $upload_info = $this->Model->saveFile($file, 'cover');
      $image = $upload_info["save_path"];
    }else{
      $width = 200;
      $height = 200;
    }
    $this->assign("backcall", $backcall);
    $this->assign("backcall_name", $backcall_name);
    $this->assign("width", $width);
    $this->assign("height", $height);
    $this->assign("image", $image);
    return $this->fetch();
  }

  public function add(){
    $backcall = $this->request->param("backcall");
    $backcall_name = $this->request->param("backcall_name");
    $width = $this->request->param("width");
    $height = $this->request->param("height");
    $id = $this->request->param("id");
    if(intval($id) > 0){
      $upload = $this->Model->findWithUser($id);
    }
    if(!empty($upload)){
      $image = $upload["save_path"];
    }else{
      $image = "";
    }

    if($this->request->isPost()){
      $file = $this->request->file('image');
      $upload_info = $this->Model->saveFile($file, 'cover');
      $image = $upload_info["save_path"];
      $id = $upload_info["id"];
    }
    $this->assign("backcall", $backcall);
    $this->assign("backcall_name", $backcall_name);
    $this->assign("width", $width);
    $this->assign("height", $height);
    $this->assign("image", $image);
    $this->assign("id", $id);
    return $this->fetch();
  }

  public function ueditor(){
    $config_file_path = ROOT_PATH."/application/ueditor.json";
    $CONFIG = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents($config_file_path)), true);

    $param_action = $this->request->param("action");
    switch ($param_action) {
    case 'config':
      $result = json_encode($CONFIG);
      break;
    /* 上传图片 */
    case 'uploadimage':
    /* 上传涂鸦 */
    case 'uploadscrawl':
    /* 上传视频 */
    case 'uploadvideo':
    /* 上传文件 */
    case 'uploadfile':
      $file = $this->request->file('file');
      $upload_info = $this->Model->saveFile($file, 'ueditor');
      $res = [
              "originalName" => $upload_info["file_name"], 
              "name" => $upload_info["file_name"], 
              "url" => $upload_info["save_path"], 
              "size" => $upload_info["size"],
              "type" => ".".$upload_info["ext"], 
              "state" => "SUCCESS"];
      $result = json_encode($res);
      break;
    /* 列出图片 */
    case 'listimage':
      $start = $this->request->param("start");
      $size = $this->request->param("size");
      $res = $this->Model->ueditorList($start, $size);
      $result = json_encode($res);
      break;
    default:
      $result = json_encode(array(
        'state'=> '请求地址出错'
      ));
      break;
    }

    echo($result);
  }

  public function umeditor(){
    if($this->request->isPost()){
      $file = $this->request->file('upfile');
      $upload_info = $this->Model->saveFile($file, 'ueditor');
      $res = [
              "originalName" => $upload_info["file_name"], 
              "name" => $upload_info["file_name"], 
              "url" => $upload_info["save_path"], 
              "size" => $upload_info["size"],
              "type" => ".".$upload_info["ext"], 
              "state" => "SUCCESS"];
      echo(json_encode($res));
    }
  }
}
