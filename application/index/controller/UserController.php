<?php
namespace app\index\controller;
use think\Session;
use app\index\controller\BaseController;
class UserController extends BaseController{
  public function _initialize($skip_action = []){
    parent::_initialize();

    $current_action = $this->request->action();
    $ctrl_name = $this->request->controller();
    $ctrl_name = strtolower($ctrl_name);
    if(!in_array($ctrl_name, array("star_log"))){
      Session::flash("back_url", $this->request->url());
    }

    if(empty($this->current_user) && !in_array($current_action, $skip_action)){
      if($this->request->isGet()){
        $this->redirect("login/login");
      }else{
        $this->success("请登录系统", "login/login", '', 0);
      }
    }
  }
}