<?php
namespace app\weixin\controller;
use think\Controller;
use think\Request;
use think\Config;
class BaseController extends Controller{
  public function _initialize($skip_action = []){
    parent::_initialize();

    // 当前页面记录
    $this->request = Request::instance();
    //当前登录用户
    $this->current_user = login_member();
    $this->assign("current_user", $this->current_user);
  }
}