<?php
namespace app\weixin\controller;
use app\weixin\controller\BaseController;
use app\weixin\model\WeixinApp;
// js SDK关相方法
class JsController extends BaseController {
  public function _initialize($skip_action = []){
    $skip_action = [];
    parent::_initialize($skip_action);
    
    $this->appid = $this->request->param("appid");
    if(empty($this->appid)){
      exit();
    }else{
      $this->Model = new WeixinApp();
    }
  }

  //获取用户的js ticket
  public function index(){
    $ticket_info = $this->Model->getJsTicket($this->appid);
    return json($ticket_info);
  }

  //验证用户的JSSDK授权--返回JSSDK授权的access_token
  public function auth(){
    $token_info = $this->Model->getJsAuth($this->appid);
    return json($token_info);
  }

  //获取用户的js ticket
  public function ticket(){
    $ticket_info = $this->Model->getJsTicket($this->appid);
    return json($ticket_info);
  }

  //获取微信jssdk的签名结果
  public function sign(){
    $url = $this->request->param("url");
    $sign_package = $this->Model->getJsSign($this->appid, $url);
    if($sign_package["errcode"]){
      $code = -1;
      $val = $sign_package["errcode"];
    }else{
      $code = 1;
      $val = $sign_package;
    }

    $result = array("state" => $code, "signinfo" => $val);
    return json($result);
  }

  //清空当前JSSDK的access_token(强制使当前JSSDK的access_token过期)
  public function reset_auth(){
    $res = $this->Model->resetJsAuth($this->appid);
    $result = array("state" => $res);
    return json($result);
  }

  //清空用户的js ticket(强制使当前JSSDK的js ticket过期)
  public function reset_ticket(){
    $res = $this->Model->resetJsTicket($this->appid);
    $result = array("state" => $res);
    return json($result);
  }
}