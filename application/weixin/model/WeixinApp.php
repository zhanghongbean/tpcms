<?php
// 微信APP
// +----------------+--------------+------+-----+-------------+----------------+
// | Field          | Type         | Null | Key | Default     | Extra          |
// +----------------+--------------+------+-----+-------------+----------------+
// | id             | int(11)      | NO   | PRI | NULL        | auto_increment |
// | name           | varchar(40)  | YES  |     |             |                |
// | type           | varchar(20)  | YES  |     |             |                |
// | appid          | varchar(40)  | YES  |     |             |                |
// | appsecret      | varchar(40)  | YES  |     |             |                |
// | token          | varchar(50)  | YES  |     |             |                |
// | encodingaeskey | varchar(50)  | YES  |     |             |                |
// | auth_scope     | varchar(15)  | YES  |     | snsapi_base |                |
// | auth_inside    | tinyint(1)   | YES  |     | 1           |                |
// | auth_callback  | varchar(255) | YES  |     |             |                |
// | editor_id      | int(11)      | YES  |     | 0           |                |
// | create_time    | int(11)      | YES  |     | 0           |                |
// | update_time    | int(11)      | YES  |     | 0           |                |
// +----------------+--------------+------+-----+-------------+----------------+
namespace app\weixin\model;
use think\Model;
use think\Loader;
use think\Config;
use app\weixin\model\TPWechat;
class WeixinApp extends Model {
  protected $auto = ['editor_id'];

  /**
  * 验证JSSDK授权
  * @param $openid string 用户微信授权key
  * @return array
  */
  public function getJsAuth($appid){
    $chat = new TPWechat($appid);
    $access_token = $chat->checkAuth();
    if($access_token){
      $result = ["access_token" => $access_token];
    }else{
      $result = ["errcode" => 80101, "errmsg" => "验证JSSDK授权失败", "res" => $access_token];
    }
    return $result;
  }

  /**
  * 重置JSAPI授权TICKET
  * @param $openid string 用户微信授权key
  * @return bool
  */
  public function resetJsAuth($appid){
    $chat = new TPWechat($appid);
    $state = $chat->resetAuth();
    if($state){
      $result = ["state" => 1];
    }else{
      $result = ["errcode" => 90301, "errmsg" => "重置JSAPI授权TICKET失败"];
    }
    return $result;
  }

  /**
  * 获取JSAPI授权TICKET
  * @param $openid string 用户微信授权key
  * @return array
  */
  public function getJsTicket($appid){
    $chat = new TPWechat($appid);
    $js_ticket = $chat->getJsTicket();
    if($js_ticket){
      $result = ["state" => 1, "js_ticket" => $js_ticket];
    }else{
      $result = ["errcode" => 90201, "errmsg" => "获取JsTicket失败"];
    }
    return $result;
  }

  /**
  * 重置JSAPI授权TICKET
  * @param $openid string 用户微信授权key
  * @return bool
  */
  public function resetJsTicket($appid){
    $chat = new TPWechat($appid);
    $state = $chat->resetJsTicket();
    if($state){
      $result = ["state" => 1];
    }else{
      $result = ["errcode" => 90301, "errmsg" => "重置JSAPI授权TICKET失败"];
    }
    return $result;
  }

  /**
  * 获取JsApi使用签名
  * @param $openid string 用户微信授权key
  * @return bool
  */
  public function getJsSign($appid, $url, $timestamp=0, $noncestr=''){
    $chat = new TPWechat($appid);
    $sign_package = $chat->getJsSign($url, $timestamp, $noncestr);
    if($sign_package){
      $result = $sign_package;
    }else{
      $result = ["errcode" => 90601, "errmsg" => "获取JsApi使用签名失败"];
    }
    return $result;
  }
}