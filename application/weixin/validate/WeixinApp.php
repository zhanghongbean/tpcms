<?php
// 微信APP
// +----------------+--------------+------+-----+-------------+----------------+
// | Field          | Type         | Null | Key | Default     | Extra          |
// +----------------+--------------+------+-----+-------------+----------------+
// | id             | int(11)      | NO   | PRI | NULL        | auto_increment |
// | name           | varchar(40)  | YES  |     |             |                |
// | type           | varchar(20)  | YES  |     |             |                |
// | appid          | varchar(40)  | YES  |     |             |                |
// | appsecret      | varchar(40)  | YES  |     |             |                |
// | token          | varchar(50)  | YES  |     |             |                |
// | encodingaeskey | varchar(50)  | YES  |     |             |                |
// | auth_scope     | varchar(15)  | YES  |     | snsapi_base |                |
// | auth_inside    | tinyint(1)   | YES  |     | 1           |                |
// | auth_callback  | varchar(255) | YES  |     |             |                |
// | editor_id      | int(11)      | YES  |     | 0           |                |
// | create_time    | int(11)      | YES  |     | 0           |                |
// | update_time    | int(11)      | YES  |     | 0           |                |
// +----------------+--------------+------+-----+-------------+----------------+
namespace app\weixin\validate;
use think\Validate;
class WeixinApp extends Validate {
  protected $rule = [
    "name" => "require",
    "type" => "require",
    "appid" => "require",
    "appsecret" => "require",
    "auth_scope" => "require",
  ];

  protected $field = [
    "name"  => "公众号名称",
    "type"  => "公众号类型",
    "appid"  => "公众号APP ID",
    "appsecret"  => "公众号APP Secret",
    "auth_scope"  => "公众号授权类型",
  ];

  protected $message = [
    "name.require" => "公众号名称不能为空",
    "type.require" => "公众号类型不能为空",
    "appid.require" => "公众号APP ID不能为空",
    "appsecret.require" => "公众号APP Secret不能为空",
    "auth_scope.require" => "公众号授权类型不能为空",
  ];
}