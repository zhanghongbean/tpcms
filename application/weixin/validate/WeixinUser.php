<?php
// 微信用户
// +---------------+--------------+------+-----+---------+----------------+
// | Field         | Type         | Null | Key | Default | Extra          |
// +---------------+--------------+------+-----+---------+----------------+
// | id            | int(11)      | NO   | PRI | NULL    | auto_increment |
// | app_id        | int(11)      | YES  |     | 0       |                |
// | openid        | varchar(50)  | YES  |     |         |                |
// | access_token  | varchar(200) | YES  |     |         |                |
// | expired_at    | datetime     | YES  |     | NULL    |                |
// | refresh_token | varchar(200) | YES  |     |         |                |
// | scope         | varchar(50)  | YES  |     |         |                |
// | subscribe     | int(11)      | YES  |     | 0       |                |
// | nickname      | varchar(255) | YES  |     |         |                |
// | sex           | int(11)      | YES  |     | 0       |                |
// | language      | varchar(10)  | YES  |     |         |                |
// | city          | varchar(100) | YES  |     |         |                |
// | province      | varchar(100) | YES  |     |         |                |
// | country       | varchar(100) | YES  |     |         |                |
// | headimgurl    | varchar(255) | YES  |     |         |                |
// | subscribed_at | datetime     | YES  |     | NULL    |                |
// | editor_id     | int(11)      | YES  |     | 0       |                |
// | create_time   | int(11)      | YES  |     | 0       |                |
// | update_time   | int(11)      | YES  |     | 0       |                |
// +---------------+--------------+------+-----+---------+----------------+
namespace app\weixin\validate;
use think\Validate;
class WeixinUser extends Validate {
  protected $rule = [
    "app_id" => "gt:0",
    "openid" => "require",
  ];

  protected $field = [
    "app_id"  => "所属APP",
    "openid"  => "微信用户OpenID",
  ];

  protected $message = [
    "app_id.gt" => "所属APP不能为空",
    "openid.require" => "微信用户OpenID不能为空",
  ];
}