<?php

use Phinx\Migration\AbstractMigration;

class AdminLog extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('admin_log',array('engine'=>'MyISAM'));
        $table->addColumn('module', 'string', array('limit' => 30, 'default' => ''))
              ->addColumn('controller', 'string', array('limit' => 30, 'default' => ''))
              ->addColumn('action', 'string', array('limit' => 30, 'default' => ''))
              ->addColumn('user_id', 'integer', array('default' => 0))
              ->addColumn('user_name', 'string', array('limit' => 30, 'default' => ''))
              ->addColumn('querystring', 'string', array('default' => ''))
              ->addColumn('ip', 'integer', array('default' => 0))
              ->addColumn('create_time', 'integer', array('default' => 0))
              ->save();
    }

    public function down(){
        $this->dropTable('admin_log');
    }
}
