<?php

use Phinx\Migration\AbstractMigration;

class Auth extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $auth_rule = $this->table('auth_rule',array('engine'=>'MyISAM'));
      $auth_rule->addColumn('name', 'string', array('limit' => 30, 'default' => ''))
                ->addColumn('title', 'string', array('limit' => 30, 'default' => ''))
                ->addColumn('type', 'boolean', array('default' => '1'))
                ->addColumn('status', 'boolean', array('default' => '1'))
                ->addColumn('condition', 'string', array('limit' => 30, 'default' => ''))
                ->addColumn('create_time', 'integer', array('default' => 0))
                ->addColumn('update_time', 'integer', array('default' => 0))
                ->save();

      $auth_group = $this->table('auth_group',array('engine'=>'MyISAM'));
      $auth_group->addColumn('name', 'string', array('limit' => 30, 'default' => ''))
                ->addColumn('title', 'string', array('limit' => 30, 'default' => ''))
                ->addColumn('type', 'boolean', array('default' => '1'))
                ->addColumn('rules', 'string', array('limit' => 200, 'default' => ''))
                ->addColumn('create_time', 'integer', array('default' => 0))
                ->addColumn('update_time', 'integer', array('default' => 0))
                ->save();

      $group_access = $this->table('group_access',array('engine'=>'MyISAM'));
      $group_access->addColumn('uid', 'integer', array('default' => '0'))
                  ->addColumn('group_id', 'integer', array('default' => '0'))
                  ->addColumn('create_time', 'integer', array('default' => 0))
                  ->addColumn('update_time', 'integer', array('default' => 0))
                  ->addIndex(array('uid'))
                  ->addIndex(array('group_id'))
                  ->save();
    }

    public function down()
    {
        $this->dropTable('auth_rule');
        $this->dropTable('auth_group');
        $this->dropTable('group_access');
    }
}
