<?php

use Phinx\Migration\AbstractMigration;

class User extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $auth_rule = $this->table('user',array('engine'=>'InnoDB'));
      $auth_rule->addColumn('name', 'string', array('limit' => 60, 'default' => ''))
                ->addColumn('password', 'string', array('limit' => 64, 'default' => ''))
                ->addColumn('nickname', 'string', array('limit' => 50, 'default' => ''))
                ->addColumn('email', 'string', array('limit' => 100, 'default' => ''))
                ->addColumn('url', 'string', array('limit' => 100, 'default' => ''))
                ->addColumn('avatar', 'string', array('limit' => 200, 'default' => ''))
                ->addColumn('gender', 'integer', array('default' => '0'))
                ->addColumn('birthday', 'date')
                ->addColumn('signature', 'string')
                ->addColumn('last_login_ip', 'integer', array('default' => '0'))
                ->addColumn('last_login_time', 'integer', array('default' => '0'))
                ->addColumn('activation_key', 'string', array('limit' => 60, 'default' => ''))
                ->addColumn('status', 'boolean', array('default' => '1'))
                ->addColumn('score', 'integer', array('default' => '0'))
                ->addColumn('user_type', 'string', array('limit' => 20, 'default' => ''))
                ->addColumn('coin', 'integer', array('default' => '0'))
                ->addColumn('mobile', 'string', array('limit' => 20, 'default' => ''))
                ->addColumn('create_time', 'integer', array('default' => 0))
                ->addColumn('update_time', 'integer', array('default' => 0))
                ->save();
    }

    public function down(){
      $this->dropTable('user');
    }
}
