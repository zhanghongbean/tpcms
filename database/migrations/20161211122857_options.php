<?php

use Phinx\Migration\AbstractMigration;

class Options extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $auth_rule = $this->table('options',array('engine'=>'InnoDB'));
      $auth_rule->addColumn('name', 'string', array('limit' => 60, 'default' => ''))
                ->addColumn('value', 'text')
                ->addColumn('autoload', 'boolean', array('default' => '1'))
                ->addColumn('create_time', 'integer', array('default' => 0))
                ->addColumn('update_time', 'integer', array('default' => 0))
                ->save();
    }

    public function down()
    {
      $this->dropTable('options');
    }
}
