<?php

use Phinx\Migration\AbstractMigration;

class AuthParent extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $auth_rule = $this->table('auth_rule');
      $auth_rule->addColumn('parent_id', 'integer', array('default' => '0'))
                ->addColumn('icon', 'string', array('limit' => 50, 'default' => ''))
                ->addColumn('islink', 'boolean', array('default' => '1'))
                ->addColumn('sort', 'integer', array('default' => '0'))
                ->addColumn('tips', 'text')
                ->save();
    }

    public function down()
    {
      $auth_rule = $this->table('auth_rule');
      $auth_rule->removeColumn('parent_id')
                ->removeColumn('icon')
                ->removeColumn('islink')
                ->removeColumn('sort')
                ->removeColumn('tips')
                ->save();
    }
}
