<?php

use Phinx\Migration\AbstractMigration;

class RenameAuthGroupType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $table = $this->table('auth_group');
      $table->addColumn('status', 'boolean', array('default' => '1', 'null' => true))
            ->addColumn('group_type', 'string', array('limit' => 30, 'default' => '', 'null' => true))
            ->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
            ->removeColumn('type')
            ->save();
    }

    public function down()
    {
      $table = $this->table('auth_group');
      $table->removeColumn('status')
                ->removeColumn('group_type')
                ->removeColumn('editor_id')
                ->addColumn('type', 'boolean', array('default' => '1', 'null' => true))
                ->save();
    }
}
