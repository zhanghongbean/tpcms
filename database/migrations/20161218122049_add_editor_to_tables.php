<?php

use Phinx\Migration\AbstractMigration;

class AddEditorToTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('user');
        $table->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
            ->save();

        $table = $this->table('auth_rule');
        $table->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
            ->save();

        $table = $this->table('group_access');
        $table->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
            ->save();

        $table = $this->table('options');
        $table->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
            ->save();
    }

    public function down(){
        $table = $this->table('user');
        $table->removeColumn('editor_id')->save();

        $table = $this->table('auth_rule');
        $table->removeColumn('editor_id')->save();

        $table = $this->table('group_access');
        $table->removeColumn('editor_id')->save();

        $table = $this->table('options');
        $table->removeColumn('editor_id')->save();
    }
}
