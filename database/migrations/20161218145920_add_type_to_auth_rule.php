<?php

use Phinx\Migration\AbstractMigration;

class AddTypeToAuthRule extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('auth_rule');
        $table->addColumn('rule_type', 'string', array('default' => '', 'limit' => 20, 'null' => true))
            ->changeColumn('tips', 'text', array('null' => true))
            ->changeColumn('sort', 'integer', array('default' => '0', 'null' => true))
            ->changeColumn('parent_id', 'integer', array('default' => '0', 'null' => true))
            ->changeColumn('icon', 'string', array('default' => '', 'limit' => 50, 'null' => true))
            ->changeColumn('condition', 'string', array('default' => '', 'limit' => 100, 'null' => true))
            ->changeColumn('status', 'boolean', array('default' => '1', 'null' => true))
            ->changeColumn('type', 'boolean', array('default' => '1', 'null' => true))
            ->changeColumn('title', 'string', array('default' => '', 'limit' => 30, 'null' => true))
            ->changeColumn('name', 'string', array('default' => '', 'limit' => 50, 'null' => true))
            ->save();
    }

    public function down()
    {
        $table = $this->table('auth_rule');
        $table->removeColumn('rule_type')->save();
    }
}
