<?php

use Phinx\Migration\AbstractMigration;

class ModifyUserColumn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('user');
        $table->changeColumn('nickname', 'string', array('default' => '', 'limit' => 50, 'null' => true))
            ->changeColumn('email', 'string', array('default' => '', 'limit' => 50, 'null' => true))
            ->changeColumn('url', 'string', array('default' => '', 'limit' => 100, 'null' => true))
            ->changeColumn('avatar', 'string', array('default' => '', 'limit' => 200, 'null' => true))
            ->changeColumn('gender', 'string', array('default' => '', 'limit' => 10, 'null' => true))
            ->changeColumn('birthday', 'date', array('null' => true))
            ->changeColumn('signature', 'string', array('default' => '', 'limit' => 200, 'null' => true))
            ->changeColumn('last_login_ip', 'integer', array('default' => '0', 'null' => true))
            ->changeColumn('last_login_time', 'integer', array('default' => '0', 'null' => true))
            ->changeColumn('activation_key', 'string', array('default' => '', 'limit' => 60, 'null' => true))
            ->changeColumn('status', 'boolean', array('default' => '1', 'null' => true))
            ->changeColumn('score', 'integer', array('default' => '0', 'null' => true))
            ->changeColumn('user_type', 'string', array('default' => '', 'limit' => 20, 'null' => true))
            ->changeColumn('coin', 'integer', array('default' => '0', 'null' => true))
            ->changeColumn('mobile', 'string', array('default' => '', 'limit' => 20, 'null' => true))
            ->changeColumn('create_time', 'integer', array('default' => '0', 'null' => true))
            ->changeColumn('update_time', 'integer', array('default' => '0', 'null' => true))
            ->save();
    }

    public function down()
    {

    }
}
