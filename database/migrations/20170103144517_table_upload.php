<?php

use Phinx\Migration\AbstractMigration;

class TableUpload extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
      $table = $this->table('upload',array('engine'=>'MyISAM'));
      $table->addColumn('save_path', 'string', array('limit' => 200, 'default' => '', 'null' => true))
                ->addColumn('ext', 'string', array('limit' => 10, 'default' => '', 'null' => true))
                ->addColumn('mine_type', 'string', array('limit' => 30, 'default' => '', 'null' => true))
                ->addColumn('size', 'integer', array('default' => '0', 'null' => true))
                ->addColumn('sha1', 'string', array('limit' => 40, 'default' => '', 'null' => true))
                ->addColumn('md5', 'string', array('limit' => 40, 'default' => '', 'null' => true))
                ->addColumn('user_id', 'integer', array('default' => '0', 'null' => true))
                ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
                ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
                ->save();
    }
}
