<?php

use Phinx\Migration\AbstractMigration;

class AddUserIntegral extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('user');
        $table->addColumn('total_score', 'integer', array('default' => '0', 'null' => true))
            ->addColumn('total_coin', 'integer', array('default' => '0', 'null' => true))
            ->renameColumn('score', 'active_score')
            ->renameColumn('coin', 'active_coin')
            ->save();

        $table2 = $this->table('auth_group');
        $table2->renameColumn('min_point', 'min_score')
            ->renameColumn('max_point', 'max_score')
            ->save();
    }

    public function down()
    {
        $table = $this->table('user');
        $table->removeColumn('total_score')
            ->removeColumn('total_coin')
            ->renameColumn('active_score', 'score')
            ->renameColumn('active_coin', 'coin')
            ->save();

        $table2 = $this->table('auth_group');
        $table2->renameColumn('min_score', 'min_point')
            ->renameColumn('max_score', 'max_point')
            ->save();
    }
}
