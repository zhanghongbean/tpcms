<?php

use Phinx\Migration\AbstractMigration;

class CreateScoreLog extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('score_log',array('engine'=>'MyISAM'));
        $table->addColumn('user_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('total_score', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('active_score', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('inc_score', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('total_coin', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('active_coin', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('inc_coin', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('comment', 'string', array('limit' => 100, 'default' => '', 'null' => true))
              ->addColumn('related_type', 'string', array('limit' => 30, 'default' => '', 'null' => true))
              ->addColumn('related_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
              ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
              ->save();
    }

    public function down(){
        $this->dropTable('score_log');
    }
}
