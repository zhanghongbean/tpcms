<?php

use Phinx\Migration\AbstractMigration;

class AddSmsLog extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('sms_log',array('engine'=>'MyISAM'));
        $table->addColumn('user_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('mobile', 'string', array('default' => '', 'limit' => 11, 'null' => true))
              ->addColumn('sms_type', 'string', array('default' => '', 'limit' => 10, 'null' => true))
              ->addColumn('content', 'string', array('default' => '', 'null' => true))
              ->addColumn('code', 'string', array('default' => '', 'limit' => 6, 'null' => true))
              ->addColumn('send_status', 'string', array('default' => 'padding', 'limit' => 10, 'null' => true))
              ->addColumn('sms_no', 'string', array('default' => '', 'limit' => 30, 'null' => true))
              ->addColumn('expire_time', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
              ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
              ->save();
    }
}
