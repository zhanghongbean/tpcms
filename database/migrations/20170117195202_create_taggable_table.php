<?php

use Phinx\Migration\AbstractMigration;

class CreateTaggableTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      $tag = $this->table('tag',array('engine'=>'InnoDB'));
      $tag->addColumn('name', 'string', array('limit' => 50, 'default' => '', 'null' => true))
          ->addColumn('sort_num', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('is_hot', 'boolean', array('default' => '0', 'null' => true))
          ->addColumn('status', 'boolean', array('default' => '1', 'null' => true))
          ->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
          ->save();

      $tagged = $this->table('tagged',array('engine'=>'InnoDB'));
      $tagged->addColumn('tag_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('taggable_type', 'string', array('default' => '', 'limit' => 50, 'null' => true))
          ->addColumn('taggable_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
          ->save();
                
    }
}
