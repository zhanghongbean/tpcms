<?php

use Phinx\Migration\AbstractMigration;

class AddCategory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      $table = $this->table('category',array('engine'=>'InnoDB'));
      $table->addColumn('name', 'string', array('limit' => 50, 'default' => '', 'null' => true))
          ->addColumn('title', 'string', array('limit' => 50, 'default' => '', 'null' => true))
          ->addColumn('meta_type', 'string', array('limit' => 50, 'default' => '', 'null' => true))
          ->addColumn('parent_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('sort_num', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('seo_title', 'string', array('default' => '', 'null' => true))
          ->addColumn('seo_keywords', 'string', array('default' => '', 'null' => true))
          ->addColumn('seo_description', 'string', array('default' => '', 'null' => true))
          ->addColumn('template_index', 'string', array('limit' => 100, 'default' => '', 'null' => true))
          ->addColumn('template_list', 'string', array('limit' => 100, 'default' => '', 'null' => true))
          ->addColumn('template_show', 'string', array('limit' => 100, 'default' => '', 'null' => true))
          ->addColumn('is_leaf', 'boolean', array('default' => '1', 'null' => true))
          ->addColumn('status', 'boolean', array('default' => '1', 'null' => true))
          ->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
          ->save();
    }
}
