<?php

use Phinx\Migration\AbstractMigration;

class Star extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('article');
        $table->addColumn('star_count', 'integer', array('default' => '0', 'null' => true))
              ->save();

        $table = $this->table('star_log',array('engine'=>'MyISAM'));
        $table->addColumn('user_id', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('related_type', 'string', array('limit' => 50, 'default' => '', 'null' => true))
          ->addColumn('related_id', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('client_ip', 'string', array('limit' => 16, 'default' => '', 'null' => true))
          ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
          ->save();
    }
}
