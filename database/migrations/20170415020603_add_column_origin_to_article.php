<?php

use Phinx\Migration\AbstractMigration;

class AddColumnOriginToArticle extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('article');
        $table->addColumn('is_origin', 'boolean', array('default' => '0', 'null' => true))
              ->addColumn('origin_author', 'string', array('default' => '', 'limit' => 100, 'null' => true))
              ->save();
    }
}
