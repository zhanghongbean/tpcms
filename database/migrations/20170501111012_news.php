<?php

use Phinx\Migration\AbstractMigration;

class News extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      $table = $this->table('news',array('engine'=>'InnoDB'));
      $table->addColumn('category_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('name', 'string', array('limit' => 50, 'default' => '', 'null' => true))
          ->addColumn('title', 'string', array('default' => '', 'null' => true))
          ->addColumn('sort_num', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('seo_title', 'string', array('default' => '', 'null' => true))
          ->addColumn('seo_keywords', 'string', array('default' => '', 'null' => true))
          ->addColumn('seo_description', 'string', array('default' => '', 'null' => true))
          ->addColumn('description', 'string', array('default' => '', 'null' => true))
          ->addColumn('content', 'text', array('null' => true))
          ->addColumn('pc_cover_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('wap_cover_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('status', 'boolean', array('default' => '0', 'null' => true))
          ->addColumn('user_id', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
          ->addColumn('is_origin', 'boolean', array('default' => '0', 'null' => true))
          ->addColumn('origin_author', 'string', array('default' => '', 'limit' => 100, 'null' => true))
          ->addColumn('star_up', 'integer', array('default' => '0', 'null' => true))
          ->addColumn('star_down', 'integer', array('default' => '0', 'null' => true))
          ->save();
    }
}
