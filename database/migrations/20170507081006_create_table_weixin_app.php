<?php

use Phinx\Migration\AbstractMigration;

class CreateTableWeixinApp extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('weixin_app',array('engine'=>'InnoDB'));
        $table->addColumn('name', 'string', array('limit' => 40, 'default' => '', 'null' => true))
              ->addColumn('type', 'string', array('limit' => 20, 'default' => '', 'null' => true))
              ->addColumn('appid', 'string', array('limit' => 40, 'default' => '', 'null' => true))
              ->addColumn('appsecret', 'string', array('limit' => 40, 'default' => '', 'null' => true))
              ->addColumn('token', 'string', array('limit' => 50, 'default' => '', 'null' => true))
              ->addColumn('encodingaeskey', 'string', array('limit' => 50, 'default' => '', 'null' => true))
              ->addColumn('auth_scope', 'string', array('limit' => 15, 'default' => 'snsapi_base', 'null' => true))
              ->addColumn('auth_inside', 'boolean', array('default' => '1', 'null' => true))
              ->addColumn('auth_callback', 'string', array('default' => '', 'null' => true))
              ->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
              ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
              ->save();
    }

    public function down(){
        $this->dropTable('weixin_app');
    }
}
