<?php

use Phinx\Migration\AbstractMigration;

class CreateTableWeixinUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('weixin_user',array('engine'=>'InnoDB'));
        $table->addColumn('app_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('openid', 'string', array('limit' => 50, 'default' => '', 'null' => true))
              ->addColumn('access_token', 'string', array('limit' => 200, 'default' => '', 'null' => true))
              ->addColumn('expired_at', 'datetime', array('null' => true))
              ->addColumn('refresh_token', 'string', array('limit' => 200, 'default' => '', 'null' => true))
              ->addColumn('scope', 'string', array('limit' => 50, 'default' => '', 'null' => true))
              ->addColumn('subscribe', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('nickname', 'string', array('default' => '', 'null' => true))
              ->addColumn('sex', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('language', 'string', array('limit' => '10', 'default' => '', 'null' => true))
              ->addColumn('city', 'string', array('limit' => '100', 'default' => '', 'null' => true))
              ->addColumn('province', 'string', array('limit' => '100', 'default' => '', 'null' => true))
              ->addColumn('country', 'string', array('limit' => '100', 'default' => '', 'null' => true))
              ->addColumn('headimgurl', 'string', array('default' => '', 'null' => true))
              ->addColumn('subscribed_at', 'datetime', array('null' => true))
              ->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
              ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
              ->save();
    }

    public function down(){
        $this->dropTable('weixin_app');
    }
}
