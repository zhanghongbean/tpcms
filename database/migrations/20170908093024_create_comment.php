<?php

use Phinx\Migration\AbstractMigration;

class CreateComment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('comment',array('engine'=>'MyISAM'));
        $table->addColumn('user_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('belongs_type', 'string', array('limit' => 20, 'default' => '', 'null' => true))
              ->addColumn('belongs_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('title', 'string', array('limit' => 100, 'default' => '', 'null' => true))
              ->addColumn('content', 'text', array('null' => true))
              ->addColumn('ip', 'integer', array('default' => 0))
              ->addColumn('status', 'boolean', array('default' => 0))
              ->addColumn('editor_id', 'integer', array('default' => '0', 'null' => true))
              ->addColumn('create_time', 'integer', array('default' => 0, 'null' => true))
              ->addColumn('update_time', 'integer', array('default' => 0, 'null' => true))
              ->save();
    }

    public function down(){
        $this->dropTable('comment');
    }
}
